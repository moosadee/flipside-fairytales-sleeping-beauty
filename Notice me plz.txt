Ways to post about the game online...

* Make an animation with the characters?
* Comics with the characters?
* Scene illustrations?
* Ask Rose to draw the characters after playing it?
* Bio sheets for each character?
* Screenshots of the game on Instagram?
* Highlight hooks of the story...
    - The princess has to choose their own personality, undo fairy gifts
    - Ezha doesn't fit in with expectations
    - The princess chooses their own gender...
    - Date fairies
