########################################################################
#                                                                 HOME #
########################################################################
label sb_ch2:

    $ renpy.pause( 1.0 )

    "KNOCK KNOCK KNOCK"

    # Change up background to look different from before
    play music "assets/music/Somewhere Sunny.mp3"
    show bg ezha_house_inner with dissolve

    # knock knock
    show ezha pjyawn with dissolve

    Fairy_Ezha          "*yawn* {p}Who is it?"
    
    if lehina_lives_with_ezha == True:
        # show lehina pajamas
        Fairy_Lehina    "I'll get the door."

    "Messenger" "A messenger from King [King_FullName]."

    show ezha pajamas at left with move
    show guard neutral2 at right with moveinright

    if lehina_lives_with_ezha == True:
        Fairy_Ezha      "What can we do for you, sir?"
    else:
        Fairy_Ezha      "What can I do for you, sir?"
    
    "Messenger"     "The king has summoned all Magic Folk to the castle for an emergency assembly."
    Fairy_Ezha      "Why? What has happened?"
    "Messenger"     "Her highness, Princess [Princess_FullName], fell into a deep sleep on the eve of her 18th birthday and cannot be roused."
    Fairy_Ezha      "Oh no..."

    "Messenger"     "The king requests all Magic Folk to head to the castle immediately. That is all."

    hide guard with moveoutright

    if lehina_lives_with_ezha == True:
        Fairy_Ezha      "This is terrible. What happened?"
        Fairy_Lehina    "We need to get to the castle as soon as we can."
    else:
        Fairy_Ezha      "This is terrible. What happened?"
    

    window hide    
    show bg chapter_frame with fade
    centered "{color=#ffffff}{size=+20}Chapter 3: The Sleep{/size}{/color}"

    hide semishade
    hide ezha with dissolve
    show bg black with fade

    # Show fairy town?
    show ezha neutral with dissolve

    show ezha thinking

    Fairy_Ezha      "I should leave as soon as possible, but maybe I can accompany another fairy there, just to be on the safe side."
    Fairy_Ezha      "The castle guards might not let me in if I'm alone."

    menu:
        "Who should I go to the castle with?"

        "Lehina {image=assets/ui/icon-lehina.png}":
            
            $ lehina_gotoCastle = True
            $ lehina += 1
            
            hide ezha with dissolve
            
            $ ch2_gowith = "Lehina"
                        
            if lehina_lives_with_ezha == True:
                Fairy_Ezha      "Are you ready to go, my {i}meénan{/i}?"
                Fairy_Lehina    "Of course. Let's hurry, my {i}thede{/i}."
                jump sb_ch2_castle
                
            else:
                Fairy_Ezha      "I'll stop by Lehina's home on the way to the castle."
                jump sb_ch2_lehina



        "Oyimahina {image=assets/ui/icon-oyimahina.png}":
            
            $ oyimahina_gotoCastle = True
            $ oyimahina += 1
            
            hide ezha with dissolve

            if lehina_lives_with_ezha == True:
                Fairy_Ezha      "I'll meet you at the castle, my {i}meénan{/i}. I'm going check if [Oyimahina_Name] has left yet."
                Fairy_Lehina    "Alright, my {i}thede{/i}."
            else:
                Fairy_Ezha "I'll stop by [Oyimahina_Name]'s home on the way to the castle."

            $ ch2_gowith = "Oyimahina"
            jump sb_ch2_oyimahina


        "Shahina {image=assets/ui/icon-shahina.png}":
            
            $ shahina_gotoCastle = True
            $ shahina += 1
            
            hide ezha with dissolve

            if lehina_lives_with_ezha == True:
                Fairy_Ezha      "I'll meet you at the castle, my {i}meénan{/i}. I'm going check if [Shahina_Name] has left yet."
                Fairy_Lehina    "Alright, my {i}thede{/i}."
            else:
                Fairy_Ezha "I'll stop by [Shahina_Name]'s home on the way to the castle."

            $ ch2_gowith = "Shahina"
            jump sb_ch2_shahina




########################################################################
#                                                       LEHINA'S HOUSE #
########################################################################
label sb_ch2_lehina:
    hide ezha with dissolve

    show ezha neutral at right with moveinright

    # Knock knock knock

    Fairy_Ezha      "[Lehina_Name], are you there?"

    Fairy_Lehina    "Oh!{p}[Ezha_Name]!{p}Yes, I'm just about ready!"

    #"After a few moments, [Lehina_Name] emerges from her house."
    show lehina neutral at left with moveinleft

    Fairy_Lehina    "I was just thinking about stopping by to check -- {w=1.0}were you also summoned?"
    Fairy_Ezha      "Yes, they summoned all of us."
    Fairy_Lehina    "Oh!{w} Good!"
    Fairy_Lehina    "We could use all the help we can get.{p}I'm glad you're coming, too."
    Fairy_Ezha      "Do you know what might be wrong with the royal heir?"
    Fairy_Lehina    "No! I'm really worried..."
    Fairy_Ezha      "Well, there's nothing we can do from here.{p}Let's hurry up."
    Fairy_Lehina    "Yes, lets!"

    jump sb_ch2_castle


########################################################################
#                                                    OYIMAHINA'S HOUSE #
########################################################################
label sb_ch2_oyimahina:
    hide ezha with dissolve

    show oyimahina neutral at left
    with dissolve

    # "I catch up to [Oyimahina_Name], already down the path from her home, walking with haste."

    Fairy_Ezha      "[Oyimahina_Name]! I'm coming with you!"
    Fairy_Oyimahina "You have also been summoned to the castle?"
    Fairy_Ezha      "Yes! I thought I might have a better chance of getting in if I'm with you."
    Fairy_Oyimahina "I am surprised that the royal family has decided to ask for you. This must be something difficult to fix."
    Fairy_Ezha      "Yeah -- *huff*"
    Fairy_Ezha      "You are the *puff* finest fairy in [Juvela_Kingdom]."
    Fairy_Oyimahina "I have such skill because I have worked hard to --"
    Fairy_Ezha      "*huff*{w} *puff*{w}"
    Fairy_Oyimahina "...Would you prefer if I walk at a slower pace?"
    Fairy_Ezha      "Yes...{w=1.0} please..."

    jump sb_ch2_castle


########################################################################
#                                                      SHAHINA'S HOUSE #
########################################################################
label sb_ch2_shahina:
    hide ezha with dissolve

    show shahina neutral at left
    with dissolve

    Fairy_Ezha      "[Shahina_Name]! Do you need help?"
    Fairy_Shahina   "Oh!{p}Uh, yes, actually."
    Fairy_Shahina   "I thought we may need this or that to help...{p}The messenger didn't really give any specifics, so..."
    Fairy_Ezha      "You're always so prepared, [Shahina_Name]. I'm glad you've been taking care of the royal family these years."
    Fairy_Shahina   "Yes well..."
    Fairy_Shahina   "It would have been nice to not be so short-handed..."
    
    if lehina_lives_with_ezha == True:
        Fairy_Ezha      "I'm sorry, [Shahina_Name]. I know it's harder for two fairies to pull the weight that four once did together."
        Fairy_Shahina   "It was bad enough being short one fairy, but since [Lehina_Name]--"
        Fairy_Shahina   "Oh, nevermind. We need to get to the castle."
        
    else:
        Fairy_Ezha      "I'm sorry, [Shahina_Name]. I know it's my fault you three have been working so much harder."
        Fairy_Shahina   "Well...{p}What's done is done.{p}At least you're pulling your weight outside the kingdom."
        Fairy_Ezha      "I try!"
    
    jump sb_ch2_castle
