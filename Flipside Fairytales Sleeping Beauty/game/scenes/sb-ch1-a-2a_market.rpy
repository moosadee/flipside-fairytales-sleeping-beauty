########################################################################
#                                                               MARKET #
########################################################################
label sb_ch1_market:
    $ shahina_seeAtMarket = True
    $ shahina += 1

    play music "assets/music/Juvela_Village.ogg"
    show bg village_market with dissolve
    show ezha bewildered with dissolve

    Fairy_Ezha "Woah! So many people! I hope I can find what I need!"

    # Make it seem like Ezha is walking by moving the whole scene over
    show shopkeeper neutral at left with moveinleft

    show ezha nervous

    Fairy_Ezha "Excuse me ma'am, do you have any fresh apples?"
    
    show ezha nervous2
    show shopkeeper talking
    
    Shopkeeper "Sorry ma'am, we're out of our stock."

    show shopkeeper neutral
    show ezha bewildered

    Fairy_Ezha "Really? Why is that?"

    show ezha bewildered2
    show shopkeeper talking
    
    Shopkeeper "The royal family is having a banquet for their new daughter."

    show shopkeeper neutral
    show ezha thinking2

    Fairy_Ezha "The king and queen had a child?{p}How did I not know this??"
    
    show ezha thinking
    
    hide shopkeeper with moveoutleft

    show ezha thinking2
    Fairy_Ezha "I guess I'll check some other stalls..."
    
    hide ezha with moveoutleft
    
    $ renpy.pause( 1.0 )
    
    show ezha bewildered with moveinleft
    
    Fairy_Ezha "Man, there's nothing here. Everything's been sold!"
    show ezha bewildered2
    Fairy_Ezha "..."
    
    $ renpy.pause( 1.0 )
    
    show ezha wave
    Fairy_Ezha "OH HEY! I see my buddy over there!"

    Fairy_Ezha "Shahina!"

    # sliding perspective trick again
    show shahina side at left with moveinleft

    Fairy_Shahina "Huh?"

    show ezha smile
    show shahina uncomfy
    Fairy_Ezha "Hey [Shahina_Nickname]!{p}Wow, such a crowd today, huh?"

    show ezha neutral
    show shahina nervous3

    Fairy_Shahina "Yes... such a big crowd!" # look nervous

    show shahina nervous4
    show ezha thinking2

    Fairy_Ezha "Are you going to the banquet tonight?"

    show shahina nervous
    show ezha thinking

    show shahina nervous3
    Fairy_Shahina "Oh!" 
    show shahina nervous
    Fairy_Shahina "Well..."
    show shahina nervous2
    Fairy_Shahina "Yes, I am..." # shocked

    show ezha thinking2

    Fairy_Ezha "When did the king and queen have a child?"

    show shahina nervous2

    Fairy_Shahina "*squirms*"

    show shahina nervous3

    Fairy_Shahina "Erm, not too long ago... They had been trying for a while now."

    show shahina nervous4
    show ezha smile

    Fairy_Ezha "Great! I'm so happy to hear! When do we get together to bless the child with gifts?"

    show shahina annoyed
    
    Fairy_Shahina "...mrph..."
    
    show ezha nervous
    
    Fairy_Ezha "Hey... are you okay?"
    
    Fairy_Ezha "..."
    
    $ renpy.pause( 1.0 )
    
    play sound "assets/sounds/460042__dbbaker01__piano-shock-impact.wav"

    # Shock sound

    show ezha bewildered with hpunch

    Fairy_Ezha "...Is that tonight? At the banquet?"

    show shahina nervous2
    show ezha stunned2

    Fairy_Shahina "Well... You see..."

    show ezha stunned

    Fairy_Ezha "Nobody told me! I haven't had time to prepare!"

    show shahina annoyed2
    show ezha stunned2
    
    Fairy_Shahina "Ezh... Uh..."
    
    show shahina annoyed
    show ezha thinking2
    
    Fairy_Ezha "What is it, [Shahina_Nickname]?"

    show ezha thinking
    
    show shahina nervous2
    
    $ renpy.pause( 5.0 )
    
    show shahina nervous3
    
    Fairy_Shahina "Yeah...{w=1.0} You don't have a lot of time to prepare..."
    Fairy_Shahina "I mean... maybe it's too late to make a gift, you know?{p}Maybe you should just come and enjoy the food."
    
    show shahina nervous4
    show ezha proud
    
    Fairy_Ezha "Nonsense! As a fairy of the kingdom, it is my duty to give the royal child a gift!"
    
    show shahina nervous2
    Fairy_Shahina "I--"

    show ezha smile

    Fairy_Ezha "Well I had better get going home to prepare something! I will see you tonight!"

    show ezha neutral
    show shahina nervous

    Fairy_Shahina "OK... yeah..."
    
    show shahina nervous2
    hide ezha with dissolve
    
    $ renpy.pause( 1.0 )
    
    hide shahina with dissolve
    show bg black with dissolve

    Fairy_Ezha "Wow, I've gotta work fast if I'm going to have a gift ready for tonight!"
    Fairy_Ezha "Time to run home!"

    $ shahina += 1
    
    jump sb_ch1_home

