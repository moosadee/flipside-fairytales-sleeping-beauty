# Maybe put interludes between each trial

label sb_ch3:
    # Change dialog bg and pause screen bg?
    
    scene bg black with dissolve
    $ renpy.pause( 1.0 )
    # $ frame = "assets/ui/frame3.png"
    # $ style.window.background = Frame(frame, 32, 32, tile=True)

    window hide    
    show bg chapter_frame with fade
    centered "{color=#ffffff}{size=+20}Chapter 4: The Four Trials{/size}{/color}"

    #hide semishade
    #show bg black with fade

# Now you play as the princess in her mind, she has to go up against four obstacles:

#   First, she has to go against Bravery because doing the rest of the tests will be more authentic that way.

#   * Bravery       -   An immortal dragon the color of the potion that has no weaknesses, she has to flee.
#                       Every time she "dies" in the dream, she comes back until she learns.
#                       The dragon can only be defeated once each of its monster henchmen are destroyed.

#   * Obedience     -   Has to learn to say "no" to peoples' demands of her...
#                       Some monster is sad that 'princesses never give him a chance',
#                       tries to guilt you into going with him.

#   * Kindness      -   She sees someone being harassed by a monster, and has to break
#                       her tendency for kindness in order to protect them and not just
#                       be complacent and not confront the monster.

#   * Beauty        -   Has to fight dysphoria in the form of a clinging hair monster.
#                       People see her as "princess" first, rather than herself first.


########################################################################
#                                                      TRIAL - BRAVERY #
########################################################################
label sb_ch3_trial1:
    $ tookSword = False
    $ tookTreasure = False
    
    play music "assets/music/Piece for Disaffected Piano Two.mp3"
    # Make this a cave instead
    show bg dream_path with dissolve
    show dreamprincess worried at Position(xpos=0.6) with dissolve
    Princess "Where am I?"
    show dreamprincess away
    Princess "This place...{w} It feels familiar? But I've never seen it before."

    scene bg black with fade

    # Skeleton field
    Princess "Woah, so many skeletons here.{w} What is this place?"

    menu:
        "Take sword from skeleton":
            Princess "I might need to protect myself."
            $ tookSword = True

        "Continue on":
            Princess "Hopefully I won't need to fight."

    Princess "I want to get out of this cave as quickly as possible."

    # Treasure room
    Princess "Wow, I don't think we have {i}this much{/i} treasure in our kingdom."
    Princess "Ack, this door is locked.{p}It has a geometric indentation in it..."
    Princess "Maybe...{w} I can use this ring?"
    Princess "Hah! That did the trick."

    menu:
        "Keep the ring":
            $ tookTreasure = True
            Princess "This ring sure is beautiful..."
            Princess "Whoever this treasure belongs to probably killed all those people back there.{p}I don't think they {i}deserve{/i} this ring."

        "Drop the ring":
            Princess "I don't want to be weighed down..."

    # Dragon room
    Princess "HOLY--"
    # Hides behind rock
    Princess "(Is that a dragon??)"
    # Dragon sniffs by the rock

    menu:
        "Attack the dragon" if tookSword:
            # Hops out from behind the rock,
            # lunges at dragon with sword
            # Dragon breathes fire and the princess screams.
            # Fade out
            Dragon "You're no match for me, princess."
            $ renpy.pause( 1.0 )
            jump sb_ch3_trial1

        "Hold up ring" if tookTreasure:
            # Leaps out and holds up the ring to get the dragon's attention.
            # Throws the ring, the dragon's head follows, but he quickly
            # turns back to the princess.
            Princess "Oh..."
            # The dragon swats at her.
            # Fade out
            Dragon "I won't be distracted so easily, princess."
            $ renpy.pause( 1.0 )
            jump sb_ch3_trial1

        "Stay still":
            # The dragon smells the princess. It reaches around the rock
            # and grabs her. She is in the air.
            Princess "LET{w} ME{w} GO!"
            # The dragon eats her.
            # Fade out
            Dragon "My sense of smell is quite keen, princess."
            $ renpy.pause( 1.0 )
            jump sb_ch3_trial1

        "RUN!":
            # The princess runs the fuck away. The dragon sees her but
            # it is too big to keep up with her.
            # As she weaves through the rocks, she finds a hole she can slip through.
            Princess "That was close..."
            Princess "Wuh-- I feel... dizzy..."

            # Something representing "bravery" cracks and breaks; maybe a visualiation of a gift.
            Princess "What was that?"

            # The other fairy shows up
            Fairy_Duthaha   "Princess, you have broken your first bind."
            Princess        "Duthaha? {w}What do you mean?"
            Fairy_Duthaha   "Your first trial was to break the gift of Bravery given to you as a baby.{p}You still have three more Gifts to destroy."
            Princess        "Bravery...?{p}So I can't be brave anymore?"
            Fairy_Duthaha   "You can be, but now you can also choose not to be."
            Princess        "...Choose..."
            Fairy_Duthaha   "You are your own person, Princess, but you are currently bound by fairy magic."
            Fairy_Duthaha   "Once you complete your quest here, you will be free."
            Princess        "Oh..."

            # Duthaha fades away
            Princess "Wait!"
            Princess "Ahh... Where do I go next?"

            # fade to black
