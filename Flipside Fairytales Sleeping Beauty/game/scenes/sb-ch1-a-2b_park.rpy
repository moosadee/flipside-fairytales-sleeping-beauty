########################################################################
#                                                                 PARK #
########################################################################
label sb_ch1_park:
    $ lehina_seeAtLake = True
    $ lehina += 1
    
    play music "assets/music/Juvela_Village.ogg"
    show bg village_park with dissolve
    show ezha neutral with dissolve
    
    Fairy_Ezha "Ahh, what a beautiful day!{p}The sun is shining, the light breeze feels good, and the lake is shimmering!"
    Fairy_Ezha "Oh look, and there are even some tadpoles swimming in the lake! And--"

    show ezha bewildered with hpunch
    Fairy_Ezha "OOF!"
    
    show ezha bewildered2
    show lehina smile2 at left with dissolve
    
    Fairy_Lehina "Oh no, Ezha! Are you okay?"

    show lehina smile
    show ezha smile
    
    Fairy_Ezha "Oh, Lehina! Hi!!"

    show lehina smile2
    show ezha nervous
    
    Fairy_Ezha "Yeah, I'm fine.{p}Are you okay?"
    
    show ezha nervous2
    show lehina talking
    
    Fairy_Lehina "Yep! Sorry about that, I was just distracted."

    show lehina neutral
    show ezha smile
    Fairy_Ezha "Oh it's probably my fault, I was watching the pond."

    show lehina smile2
    Fairy_Lehina "Oh, don't tell me that you're going to give the child one of those slimey things as your gift."

    show ezha nervous
    Fairy_Ezha "Huh?{w} Child?{w} Gift?"

    show lehina nervous
    Fairy_Lehina "The banquet is tonight, you know?"
    
    show lehina nervous2
    show ezha bewildered
    
    Fairy_Ezha "Banquet??"

    show lehina nervous
    show ezha bewildered2
    Fairy_Lehina "The... King and queen had a daughter?"

    show lehina nervous2 
    show ezha stunned with hpunch
    
    Fairy_Ezha "They had a kid?! Finally??"

    show ezha nervous2
    show lehina smile
    
    Fairy_Lehina "Yeah! They have a newborn baby!"
    
    show lehina smile2
    show ezha charmed
    
    Fairy_Ezha "Woah! Congratulations to them! They finally have their first child!"

    show ezha charmed2
    show lehina smile
    
    Fairy_Lehina "Yes and they're very excited."

    show lehina unsure
    Fairy_Lehina "..."

    show lehina unsure2
    Fairy_Lehina "Oh but the gifting banquet is tonight. Did you not get an invitation?"

    show lehina unsure3
    show ezha stunned
    
    Fairy_Ezha "No I had no idea! I didn't even know there WAS a baby!"
    
    show ezha stunned2
    show lehina unsure2
    
    Fairy_Lehina "Oh no! Do you think you have enough time to get a gift together?"

    show lehina unsure3
    show ezha thinking

    $ renpy.pause( 1.0 )

    show ezha nervous
    Fairy_Ezha "Yeah, I think so. I should get going home though if I want to be prepared!"

    show lehina smile
    show ezha nervous2
    
    Fairy_Lehina "Okay! I'll let you get going then. I will see you there!"

    show lehina smile2
    show ezha proud
    
    Fairy_Ezha "Yeah!"
    
    hide ezha with dissolve
    hide lehina with dissolve
    show bg black with dissolve

    Fairy_Ezha "Wow! What amazing news!"
    Fairy_Ezha "I gotta get home and get something prepared!!"

    $ lehina += 1
    
    jump sb_ch1_home
    
