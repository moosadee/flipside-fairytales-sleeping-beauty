label sb_ch4:
    show bg castle_bedroom with dissolve
    
    # Duthaha wakes up
    
    Fairy_Duthaha   "The [title] has finished their quest."
    
    window hide    
    show bg chapter_frame with fade
    centered "{color=#ffffff}{size=+20}Chapter 5: The Revival{/size}{/color}"
    
    if pronounSet == "She":
        Fairy_Duthaha   "I think we should leave her to rest, and to speak with the king and queen."
        
    elif pronounSet == "He":
        Fairy_Duthaha   "I think we should leave him to rest, and to speak with the king and queen."
        
    elif pronounSet == "They":
        Fairy_Duthaha   "I think we should leave them to rest, and to speak with the king and queen."
    
    
    
    # Fade to another room, the fairies are waiting around.
    
    Fairy_Ezha      "Ack, we've been waiting for hours. Can't we go home?"
    Fairy_Shahina   "We haven't been dismissed, so it's best to stay here."
    Fairy_Lehina    "We should at least check in once the king and queen are done."
    Fairy_Oyimahina "And I am interested in seeing the princess just to make sure that nothing else is wrong."
    
    if pronounSet == "He":
        Fairy_Duthaha   "He is \"[title]\" now, not princess."
        Fairy_Oyimahina "Oh, I see."
        
    elif pronounSet == "They":
        Fairy_Duthaha   "They are \"[title]\" now, not princess."
        Fairy_Oyimahina "Oh, I see."
        
    Fairy_Duthaha   "I'm going to talk with the king and queen, to make sure they don't undo the progress we've made."
    Fairy_Ezha      "You can go in there?"
    Fairy_Duthaha   "Whehter or not I'm welcome, I'm going to protect the [title]."
    
    Fairy_Ezha      "Well..."
    
    # You have an interaction chance with the other fairies.
    
    menu:
        Fairy_Ezha  "Gotta pass the time..."
        
        "Talk to [Lehina_Name]":
            if lehina_lives_with_ezha == True:
                call sb_ch4_talkLehina_gf from _call_sb_ch4_talkLehina_gf
                
            else:
                call sb_ch4_talkLehina from _call_sb_ch4_talkLehina
            
        "Talk to [Shahina_Name]":
            call sb_ch4_talkShahina from _call_sb_ch4_talkShahina
            
        "Talk to [Oyimahina_Name]":
            call sb_ch4_talkOyimahina from _call_sb_ch4_talkOyimahina
    
    jump sb_ch4_kingEmerges


label sb_ch4_talkLehina:
    $ lehina += 1
    $ lehina_waiting_talk = True
    
    Fairy_Ezha      "Hey [Lehina_Nickname], what are you thinking about?"
    Fairy_Lehina    "I'm mostly just happy that the [title] is awake."
    Fairy_Ezha      "Yeah..."
    Fairy_Ezha      "Is [Duthaha_Name] mad at us?"
    Fairy_Lehina    "I don't think she's mad at {i}us{/i}, per se.{p}I think she's mad at the gifting tradition."
    Fairy_Ezha      "Yeah. I haven't heard of any royal babies {i}not{/i} receiving gifts."
    Fairy_Lehina    "Me neither. But [Duthaha_Name] seems to realize osmething about it that we have not."
    Fairy_Ezha      "I feel bad about that."
    Fairy_Lehina    "Well, sometimes when we cause pain, we can't go back and {i}undo{/i} what we've done, but we can make sure we don't do it again."
    Fairy_Ezha      "Yeah, I suppose so."
    
    return
    
label sb_ch4_talkLehina_gf:
    $ lehina += 1
    $ lehina_waiting_talk = True
    
    Fairy_Ezha      "How are you faring, my {i}thede{/i}?"
    Fairy_Lehina    "I'm good, my {i}meénan{/i}. How are you feeling?"
    Fairy_Ezha      "A bit anxious!"
    Fairy_Lehina    "Me too. We all are. But, let's just wait for now."
    Fairy_Lehina    "If you want to take a nap, you can use me as a pillow."
    Fairy_Ezha      "Thanks, [Lehina_Nickname]."
    
    # Ezha rests on Lehina's lap
    
    return
    
label sb_ch4_talkShahina:
    $ shahina += 1
    $ shahina_waiting_talk = True
    
    Fairy_Ezha      "Hey [Shahina_Nickname], what's on your mind?"
    Fairy_Shahina   "Oh, not a lot... Just wondering what the king and queen are discussing."
    Fairy_Ezha      "Do you think that they're angry at the [title]?"
    Fairy_Shahina   "Maybe? I don't know how much [Duthaha_Name] is telling them about what happened."
    Fairy_Ezha      "Yeah... I'd imagine if they knew {i}she{/i} was responsible, she might get kicked out."
    Fairy_Shahina   "Well, [Duthaha_Name] is a very important healer back in [Arjento_Kingdom]."
    Fairy_Ezha      "I didn't know that."
    Fairy_Shahina   "I think that the king and queen will show her respect and trust her judgement."
    Fairy_Ezha      "Wow, I wonder what that's like..."
    
    return
    
label sb_ch4_talkOyimahina:
    $ oyimahina += 1
    $ oyimahina_waiting_talk = True
    
    Fairy_Ezha      "Hey, [Oyimahina_Nickname], how are you?"
    Fairy_Oyimahina "I am doing alright, [Ezha_Name]. How are you faring?"
    Fairy_Ezha      "I'm alright. I'm a bit anxious..."
    Fairy_Oyimahina "About waiting?"
    Fairy_Ezha      "Well, yes, but also just this whole ordeal."
    Fairy_Oyimahina "Are you talking about the gifting?"
    Fairy_Ezha      "Yeah!{p}I didn't think we were doing {i}harm{/i}. Why would we try to harm someone??"
    Fairy_Oyimahina "I had not thought about our gifts in this way until [Duthaha_Name] came here.{p}And, honestly, I see her point."
    Fairy_Oyimahina "Sometimes, we may cause harm without intending that to happen."
    Fairy_Ezha      "Well, how do we make up for it?"
    Fairy_Oyimahina "I guess the best thing to do is to let others tell us what {i}their{/i} needs and wants are, rather than us acting on behalf of them before they can even talk."
    Fairy_Ezha      "I guess... No more royal giftings."
    Fairy_Oyimahina "At least until the person in question is old enough to tell us themselves what they want."
    Fairy_Ezha      "Yeah!"
    
    return
    


########################################################################
#                                                         KING EMERGES #
########################################################################
label sb_ch4_kingEmerges:
    
    King    "*sigh*"
    King    "Thank you for staying, [Oyimahina_Name], [Lehina_Name], [Shahina_Name], ... [Ezha_Name]."
    King    "There is a lot for us to take care of right now, but you have my gratitude for coming and doing what you could to help."
    King    "Once the [title] is rested, we will have a more formal announcement."
    King    "But until then, you may head home."
    
    Fairy_Lehina    "Thank you, your highness."
    
    King    "Oh.{w=1.0} [Ezha_Name]."
    King    "Let me just informally tell you now, that you are no longer banished.{p}If you wish to return to our kingdom, you are free to do so."
    
    Fairy_Ezha  "Oh, thank you, King [King_Name].{p}I'm not sure what I will do right now, but I will think about it."
    
    King    "Alright. Well.{p}Have a good evening."
    
    
########################################################################
#                                                      OUTDOOR BANQUET #
########################################################################
label sb_ch4_banquet:
    
    Fairy_Lehina    "Wow, it's such a lovely day."
    Fairy_Ezha      "Hey, where is [Duthaha_Name]?"
    Fairy_Lehina    "Everyone from the [Arjento_Kingdom], including [Prince_Name] and [Duthaha_Name], have already left."
    Fairy_Ezha      "Oh..."
    Fairy_Oyimahina "I had hoped to be able to talk with [Duthaha_Name] more before she left."
    Fairy_Shahina   "At least that [Prince_Name] is gone. I did NOT like him."
    Fairy_Ezha      "Same!"
    
    # fade in, fade out
    
    Fairy_Ezha      "No sign of the royal family yet. I guess there's some time to mingle."
    
    menu:
        "Spend time with..."
        
        "[Lehina_Name]      {image=assets/ui/icon-lehina.png}":
            if lehina_lives_with_ezha:
                call sb_ch4_banquet_lehina from _call_sb_ch4_banquet_lehina
                
            else:
                call sb_ch4_banquet_lehina_gf from _call_sb_ch4_banquet_lehina_gf
            
        "[Oyimahina_Name]   {image=assets/ui/icon-oyimahina.png}":
            call sb_ch4_banquet_oyimahina from _call_sb_ch4_banquet_oyimahina
            
        "[Shahina_Name]     {image=assets/ui/icon-shahina.png}":
            call sb_ch4_banquet_shahina from _call_sb_ch4_banquet_shahina
            
    jump sb_ch4_banquet_kingSpeech
    
########################################################################
#                                                     BANQUET - LEHINA #
########################################################################
label sb_ch4_banquet_lehina:
    
    return
    
########################################################################
#                                                BANQUET - LEHINA (GF) #
########################################################################
label sb_ch4_banquet_lehina_gf:
    Fairy_Ezha      "Hey babe."
    Fairy_Lehina    "Hey {i}thede{/i}."
    Fairy_Ezha      ""
    
    return
    
########################################################################
#                                                  BANQUET - OYIMAHINA #
########################################################################
label sb_ch4_banquet_oyimahina:
    if lehina_lives_with_ezha:
        # Draw Lehina with
        pass
        
    Fairy_Ezha      "Hi [Oyimahina_Nickname]!"
    
    if oyimahina > 3:   # Oyi is fond of you
        pass
        
    else:               # Oyi is indifferent
        pass
    
    return
    
########################################################################
#                                                    BANQUET - SHAHINA #
########################################################################
label sb_ch4_banquet_shahina:
    if lehina_lives_with_ezha:
        # Draw Lehina with
        pass
    
    return
    
########################################################################
#                                                        KING'S SPEECH #
########################################################################
label sb_ch4_banquet_kingSpeech:
    
    King        "Ladies, gentlemen, and distinguished guests. Thank you for being here."
    King        "We have an announcement, regarding the royal wedding, joining the [Juvela_Kingdom] and the [Arjento_Kingdom]."
    Queen       "Through our child's decision, we have decided to nullify the engagement."
    Queen       "We are still on good terms with the [Arjento_Kingdom], but..."
    King        "We will allow our child to find their own partner."
    
    Queen       "Additionally - We are here to celebrate the health of our child."
    
    if pronounSet == "He" or title == "They" or Princess_Name != Princess_Name_Original:
        King    "And to reintroduce you to the sunshine of our life."
        
        if pronounSet == "He":
            King    "Please welcome our son, [Princess_FullName]."
            
        elif pronounSet == "They":
            King    "Please welcome our child, [Princess_FullName]."

    Queen       "We are so grateful for the help of [Duthaha_Name] and the other fairies for their aid."
    Queen       "And we are overjoyed to see our [childTitle] happy."
    
    Princess    "Thank you, everybody, for coming and supporting me."
    
    # Applause

    # If you're already in a relationship with Lehina, you can also
    # have another relationship. Is open, yo.

    
    # What will the heir do now?
    # The fairies suggest things -- go adventuring, find a suitor, etc.
    
    # The heir responds: It's up to me. :)
    
    # The end





