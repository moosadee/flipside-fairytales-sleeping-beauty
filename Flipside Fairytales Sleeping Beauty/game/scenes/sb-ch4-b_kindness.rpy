########################################################################
#                                                     TRIAL - KINDNESS #
########################################################################
label sb_ch3_trial3:
    scene bg black with fade
#   * Kindness      -   She sees someone being harassed by a monster, and has to break
#                       her tendency for kindness in order to protect them and not just
#                       be complacent and not confront the monster.

    # The princess arrives in a village.
    Princess "There's a crowd gathering over there."
    
    # The princess approaches the group
    SoapboxGuy "If we keep letting dwarves into our village, everything will go downhill!"
    SoapboxGuy "They're bringing in their customs and beliefs, and not {i}integrating{/i} into our society!"
    
    "A dwarf carrying her groceries passes by, trying to avoid eye contact."
    
    SoapboxGuy "Hey!"
    
    "The woman looks worried and tries to keep moving."
    
    "Angry woman" "He's talking to you! Don't you speak our language?"
    
    DwarfGirl "...yes..."
    
    AngryGuy "You don't belong here!"
    
    "Another angry guy" "Go back to your caves!"
    
    DwarfBoy "Leave her alone!"
    
    # A dwarven man gets between the other dwarf and the angry people
    
    DwarfBoy "Are you okay, {i}meli-pona{/i}?"
    
    AngryGuy "Stop speaking that language!"
    
    # Dwarf boy steps forward
    DwarfBoy "We have every right to be here."
    
    # Angry guy steps forward
    AngryGuy "Your people are destroying our village."
    
    Princess "I should intervene..."
    
    
    $ assertiveCounter = 0
    $ tryingToDebate = 0
  
label sb_ch3_trial3_debate:

    # Speaking up 1
    menu:
        "Please stop fighting!":                                        # Kindness
            SoapboxGuy "We have to protect our families from these outsiders!"
        
        "You both make good points.":                                   # Kindness
            DwarfBoy "What? They don't make {i}any{/i} good points, it's all hate!"
    
    SoapboxGuy "When dwarves like these move into our spaces, they take our resources!"
    
    # Speaking up 2
    menu:
        "There is plenty to go around!":                                # Trying to debate
            $ tryingToDebate += 1
            AngryGuy "We've already seen it happening! {p}They come here and only care about their own!"
            
        "Stop it!":                                                     # Trying to be assertive
            $ assertiveCounter += 1
            Princess "St-- {w} Stop it,{w} please!"
            AngryGuy "Whose side are you on?!{p}Are you a traitor to your fellow man?"
            
    DwarfGirl "We're just trying to live our lives."
    
    AngryGuy "Dwarven values are incompatible with {i}our{/i} values!"
    
    # Speaking up 3
    menu:
        "Everybody is just trying to work, take care of their families, and be happy!":     # Trying to debate
            $ tryingToDebate += 1
            SoapboxGuy "Yeah, trying to help {i}themselves{/i} while trying to destroy {i}us{/i}!"
        
        "Just leave!":
            Princess "I-- I think you should leave!"
            $ assertiveCounter += 1
            SoapboxGuy "No! I'm here to speak the truth!"

    SoapboxGuy "I say we should be doing all we can to drive out these {i}invaders{/i}!"
    AngryGuy "Yeah!!"
    
    # Speaking up 4
    menu:
        "Please leave them alone, they're not hurting anyone.":         # Kindness
            SoapboxGuy "They're hurting the village, our culture, and the economy here!"
            
        "You're not welcome here!":                                     # Trying to be assertive
            $ assertiveCounter += 1
            SoapboxGuy "{i}Us?{/i} This is OUR home!"
            
    AngryGuy "{i}THEY{/i} don't belong HERE!{p}WE are the rightful occupants!"
            

label sb_ch3_trial3_loop:
    menu:
        "Fine. Have it your way." if assertiveCounter is 0:             # Failure state
            # Screen fades to black
            "I couldn't take it anymore. I left the conflict."
            jump sb_ch3_trial3
    
        "Go away!" if assertiveCounter is 1:
            $ assertiveCounter += 1
            AngryGuy "We aren't going anywhere!"
            SoapboxGuy "This is where we belong!"
        
        "Fuck you." if assertiveCounter is 2:
            $ assertiveCounter += 1
            AngryGuy "What?{p}No, fuck YOU!"
            SoapboxGuy "Who do you think you are?!"
        
        "Punch them." if assertiveCounter is 3:                          # End of conflict
            $ assertiveCounter += 1
            jump sb_ch3_trial3_end

    jump sb_ch3_trial3_loop
    
label sb_ch3_trial3_end:
    
    # Whatever effects to show the punching.
    
    AngryGuy "Dwarf-lover!"
    SoapboxGuy "This isn't the last of this!"
    
    "The angry men leave.{p}The tension weighs heavily in the air and we don't move for a moment."
    
    DwarfBoy "Thank-- thank you.{p}For standing up for us."
    
    DwarfGirl "I'm so tired of this happening."
    
    Princess "I'm sorry you had to deal with this."
    
    DwarfGirl "I- ... I need to head home."
    
    DwarfBoy "I'll go with you."
    
    # The gift appears and shatters
    
    Fairy_Duthaha "Good job, princess.{w} You don't have to be kind to everybody."
    Princess "I've always had to be kind to everybody..."
    Princess "I haven't been able to stand up for myself before, but I was able to stand up for them."
    Fairy_Duthaha "Now you can make that decision for yourself."
    
