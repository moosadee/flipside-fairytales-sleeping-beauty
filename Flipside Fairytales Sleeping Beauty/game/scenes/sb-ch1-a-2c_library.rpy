
########################################################################
#                                                              LIBRARY #
########################################################################
label sb_ch1_library:
    $ oyimahina_seeAtLibrary = True
    $ oyimahina += 1    

    show bg black with dissolve
    
    play sound "assets/sounds/392233__gropkcor__church-door-interior-throwleigh-sel.ogg"
    
    $ renpy.pause( 1.0 )
    
    play music "assets/music/Perspectives.mp3"
    show bg village_library with dissolve
    show ezha neutral with dissolve
    
    show ezha nervous
    Fairy_Ezha "Ack, that door is heavy."
    
    show ezha charmed
    Fairy_Ezha "Oh but I love the smell of old books here."
    
    show ezha thinking2
    Fairy_Ezha "Let's see, I'd like to pick up a book on ancient fairy languages, and on patchwork potions..."
    
    hide ezha with dissolve
    
    $ renpy.pause( 1.0 )
    
    show ezha neutral with dissolve

    show ezha smile
    Fairy_Ezha "Will I read these or not? Who knows! We will see!"

    Fairy_Ezha "Time to check out!"
    
    show ezha neutral

    show oyimahina neutral at Position(xpos=-1.0)

    $ renpy.pause( 0.2 )
    
    show ezha neutral
    show oyimahina sideways at left
    with dissolve

    Fairy_Ezha "Oh, hey!"
    
    show ezha wave
    Fairy_Ezha "Good morning, [Oyimahina_Nickname]. How are you doing today?"

    show oyimahina sideways2
    show ezha neutral
    
    show ezha smile
    Fairy_Ezha "What's that book? \"Ban bini\"?" 

    show ezha charmed    
    Fairy_Ezha "Are you making a trait potion for a newborn?"

    show ezha charmed2
    show oyimahina talking

    Fairy_Oyimahina "Hello, Ezha. Yes, I need to put the finishing touches on a gift for a newborn."

    show oyimahina neutral
    show ezha charmed3 at center with move
    
    Fairy_Ezha "What an honor for the family!"
    Fairy_Ezha "Who are the parents?"

    show oyimahina talking

    Fairy_Oyimahina "The parents are King [King_Name] and Queen [Queen_Name]."

    show oyimahina neutral
    show ezha stunned with hpunch

    Fairy_Ezha "Wait, what? They've had a child?"

    show ezha stunned2
    show oyimahina talking

    Fairy_Oyimahina "Yes, the queen recently gave birth to a baby and there is a banquet tonight."
    Fairy_Oyimahina "The other fairies and I will be there tonight to bequeath our respective gifts."

    show oyimahina neutral
    show ezha crying
    
    Fairy_Ezha "I never got an invite! I haven't had any time to prepare a gift!"
    
    show ezha crying2
    show oyimahina sidelook

    Fairy_Oyimahina "That is understandable. I assume that the King and Queen did not want you to bestow what you consider a gift to their child."

    show oyimahina sideways2
    show ezha annoyed2
    
    Fairy_Ezha "That's nonsense; all kingdom fairies are given the chance to give gifts!"

    show ezha mad
    show oyimahina sideways3

    Fairy_Oyimahina "That may be so, but you might also consider whether your presence is wanted, and to consider respecting their wishes."
    
    show oyimahina sideways2
    show ezha bewildered
    
    Fairy_Ezha "Uhh..."

    show oyimahina sideways3
    
    Fairy_Oyimahina "I am leaving first.{p}Goodbye."
    
    show oyimahina sideways2
    Fairy_Ezha "Oh, okay."

    hide oyimahina sideways with moveoutleft
    show ezha at left with move

    show ezha thinking
    
    Fairy_Ezha "Hmm."
    
    hide ezha with dissolve
    hide oyimahina with dissolve
    show bg black with fade

    Fairy_Ezha "Well, this is some pretty big news!"
    Fairy_Ezha "I'm not sure if I explicitly wasn't invited, or if my invite got lost in the mail..."
    Fairy_Ezha "...But I'm a fairy, dangit, and it's my right to bequeath a gift to the royal offspring!"
    Fairy_Ezha "Plus I'm excited to see the new baby and give it a gift!!"

    $ oyimahina += 1
    
    jump sb_ch1_home
