########################################################################
#                                                               CASTLE #
########################################################################
label sb_ch2_castle:

    hide ezha
    hide lehina
    hide shahina
    hide oyimahina

    play music "assets/music/Decline.mp3"

    show bg castle_throne
    with fade

    #"A guard brings us to the throne room. There are already some Magical Folk here, waiting and whispering between themselves."

    #"The doctor arrives shortly afterward, looking very serious."

    show doctor neutral at left with moveinleft

    Doctor          "The princess has been asleep for two days now, and we cannot wake her up."
    "Someone"       "...But she's alive?"

    Doctor          "Yes, she's alive, but asleep."
    Doctor          "I've done all that I can do, but perhaps it is not enough. We were hoping that one of you would be able to revive her."

    Fairy_Oyimahina "Do you know how, or why, she fell into a sleep like this?"

    Doctor          "We do not know that, either.{p}If any of you can {i}find out{/i} the cause of this, the royal family will give you an additional reward."

    #"King Godric appears coming down the stairs from the tower above."

    show king angry at Position(xpos=0.4) with moveinleft

    King            "For the time being, all of you will be free to move about the castle. The princess is upstairs."
    King            "If you find out anything, please let us know."

    # "The king looks so worn and tired, he doesn't even acknowledge my presence. Normally, I'd at least get a dirty look!"

    hide doctor
    hide king with dissolve

    show shahina neutral at Position(xpos=0.1)
    show lehina neutral at Position(xpos=0.35)
    show ezha neutral at Position(xpos=0.65)
    show oyimahina neutral at Position(xpos=0.9)
    with dissolve

    Fairy_Shahina   "I think I'm going to check in on the princess first."
    Fairy_Oyimahina "I will go and ask the castle staff first to see if they know of anything."
    Fairy_Lehina    "I'll go comfort the king and queen for a moment before heading up."
    Fairy_Ezha      "OK! What should I do?"
    Fairy_Oyimahina "..."
    Fairy_Shahina   "You just do your own thing, I guess."

    hide shahina
    hide lehina
    hide oyimahina
    with dissolve

    # Find potion bottle
    # Talk to prince
    # Talk to handmaid

########################################################################
#                                                           SEARCH HUB #
########################################################################
label sb_ch2_searchHub:
    menu:
        "Where should I go?"

        "[Pricess_Name]'s bedroom":
            # Investigate around her room
            # The potion giver is here
            # Shahina is here
            # Doctor is here
            hide ezha with dissolve
            jump sb_ch2_searchBedroom

        "Tea room":
            # Another new magical person is here
            # Lehina is here
            # King and queen is here
            # Prince is here
            hide ezha with dissolve
            jump sb_ch2_searchTearoom

        "Staff room":
            # Talk to staff to learn about princess' emotional health
            # Oyimahina is here
            hide ezha with dissolve
            jump sb_ch2_searchStaffroom


# At the castle, you will be investigating to try to figure out what happened.
# There will be the princess' fiancee (a jerk) as well as another magical being.
# The princess took a potion given to her by the other magical being; she was
# told that it would help her defeat her anxiety caused by the Gifts bestowed upon her.
# Later on, we will go into her subconscious and defeat several trials for each Gift.

########################################################################
#                                                     CASTLE - BEDROOM #
########################################################################
label sb_ch2_searchBedroom: # Investigate room / Shahina / Doctor / Duthaha
    show bg castle_bedroom with dissolve

    "In the bedroom, the princess rests on her bed. The doctor watches over her, while Shahina experiments with her potions. Another fairy - not from our town - is sitting and watching."

label sb_ch2_searchBedroom_menu:
    menu:
        "Talk to doctor" if not talkedToDoctor:
            $ talkedToDoctor = True
            show doctor neutral at left
            show ezha neutral at right
            with dissolve

            Fairy_Ezha "What can I do to help, Doctor?"
            Doctor "I've done all I can do within my realm, so now I must defer to your expertise."
            Fairy_Ezha "Do you have any idea what caused this sleep?"
            Doctor "I am completely at a loss, I'm afraid."

            hide ezha
            hide doctor
            with dissolve

        "Talk to Shahina" if not talkedToShahina:
            $ talkedToShahina = True
            show shahina neutral at left
            show ezha neutral at right
            with dissolve

            Fairy_Ezha "Is there anything I can do to help you, Shahina?"
            Fairy_Shahina "Ohh, I don't know..."
            show shahina annoyed
            Fairy_Shahina "Aggh, nothing is working."
            menu:
                "Don't forget to take a break!":
                    Fairy_Ezha "You've been working really hard, don't forget to take a break."
                    show shahina neutral
                    Fairy_Shahina "I know...{w} I just feel bad if I can't help out."
                    Fairy_Ezha "You {i}are{/i} helping, but sometimes you need a break to give your mind room to work."
                    Fairy_Shahina "Yeah...{w} I'll join you for a break later, Ezha."
                    "Shahina looks worn out, and it makes me want to just put my arms around her and give her a big hug.{p}But I should probably let her work."
                    $ shahina += 1

                "We can't give up now!":
                    Fairy_Ezha "We can't give up now! Give it all you've got!"
                    Fairy_Shahina "Aggh, I have tried everything! I don't know what to do!!"
                    Fairy_Ezha "I'll leave you to it..."

            hide ezha
            hide shahina
            with dissolve

        "Talk to other fairy" if not talkedToDuthaha:
            $ talkedToDuthaha = True
            $ ch2_talkedToDuthaha = True
            show duthaha neutral at left
            show ezha neutral at right
            with dissolve

            Fairy_Ezha      "Hi there, where are you from?"
            Fairy_Duthaha   "I'm from [Arjento_Kingdom]. I serve prince [Prince_Name] and his family."
            Fairy_Ezha      "Ah! So you're with the fiancé's family!"
            Fairy_Duthaha   "I am."
            Fairy_Ezha      "Had you met the princess before?"
            Fairy_Duthaha   "Of course. During the engagement ceremony all the [Juvela_Kingdom] and [Arjento_Kingdom] fairies were there."
            Fairy_Ezha      "Oh, of course.{p}I {i}was{/i} a fairy serving [Juvela_Kingdom] but I haven't been welcome at the castle in quite some time."
            Fairy_Duthaha   "I thought you might be [Ezha_FullName]."
            Fairy_Ezha      "Ahaha..."
            Fairy_Duthaha   "Fairies do love to gossip."
            Fairy_Ezha      "I know...{p}*sigh*"
            
            Fairy_Duthaha   "By the way, [Ezha_Name], I am [Duthaha_FullName]."
            $ Duthaha_Name = "Duthahá"
            
            Fairy_Ezha      "So, uh, do you have any ideas about what happened to the heir...{w} or how to revive them?"
            Fairy_Duthaha   "I have an idea of what will revive her, but we have to wait and see."
            Fairy_Ezha      "What does that mean?"
            Fairy_Duthaha   "It means I am currently in the process of working on a solution."
            Fairy_Ezha      "Which is...?"
            Fairy_Duthaha   "Just be patient."
            Fairy_Ezha      "Hmm..."

            hide ezha
            hide duthaha
            with dissolve

        "Search room" if not searchedBedroom:
            $ searchedBedroom = True
            $ ch2_foundPotion = True

            #"I walk around the perimeter of the room, but don't see anything out of the ordinary."
            #"As I approach the princess, I hear the ring of glass as my foot hits something beneath the bed curtain."

            show image "assets/images/potion-bottle.png" at Position(xpos=0.5, ypos=0.7)
            #"What's this?{p}It appears to be a small potion bottle."

            hide image "assets/images/potion-bottle.png"
            #"This could be important, so I'll keep it on me."

        "Go elseware":
            jump sb_ch2_searchHub

        "Discuss evidence with fairies" if ch2_talkedToDuthaha and ch2_foundPotion and ch2_talkToHandmaid and ch2_talkToPrince:
            jump sb_ch2_evidence

    jump sb_ch2_searchBedroom_menu

########################################################################
#                                                     CASTLE - TEAROOM #
########################################################################
label sb_ch2_searchTearoom: # King / Queen / Prince / Lehina / Sprite
    show bg castle_tearoom with dissolve

    #"The king and queen are sitting together in the tea room, across from the prince - princess Amelina's fiancé. Lehina and a spritelet are also sitting here."

label sb_ch2_searchTearoom_menu:
    menu:
        "Talk to king and queen" if not talkedToKingQueen:
            $ talkedToKingQueen = True
            show king angry at left
            show queen angry at center
            show ezha neutral at right
            with dissolve

            Fairy_Ezha  "Good afternoon, your highnesses."
            Queen       "It most definitely is {i}not{/i} a good afternoon."
            Fairy_Ezha  "Oh, uh..."
            #"The king lays his hand on the queen's shoulder."
            King        "Dear..."
            King        "I am sorry, [Ezha_Name]. We are quite worried for [Princess_Name]."
            Fairy_Ezha  "I understand, I'm sorry for not thinking before speaking."
            Queen       "Is there anything you can do for her?"
            Fairy_Ezha "I'll try, your highness. I'm asking around to try to figure out {i}why{/i} this happened, and that will help me figure out how to fight this... curse,{w} disease,{w} thing..."
            King        "Thank you, [Ezha_Name]."

            #"I decide to leave them alone."

            hide ezha
            hide king
            hide queen
            with dissolve

        "Talk to prince" if not talkedToPrince:
            $ talkedToPrince = True
            $ ch2_talkToPrince = True
            
            show prince neutral at left
            show ezha neutral at right
            with dissolve
            
            Fairy_Ezha  "Prince [Prince_FullName]?"
            Prince      "Yes?"
            Fairy_Ezha  "I'm [Ezha_FullName]. I am --{w=1.0}{i}was{/i} one of the [Juvela_Kingdom]'s fairies here."
            Prince      "You weren't at our engagement..."
            Fairy_Ezha  "Well, I kind of pissed off the royal family..."
            Prince      "Oh."
            Fairy_Ezha  "What can you tell me about the heir's emotional health recently?"
            Prince      "\"Emotional health\"?{p}You don't fall asleep because you're emotionally {i}weak{/i}."
            Fairy_Ezha  "Highness, it is important-"
            Prince      "{i}What does the princess even have to worry about?{/i}{p}She's a princess! She lives in a castle."
            Fairy_Ezha  "..."
            Prince      "{i}I{/i} had to put in all the {i}hard work{/i} to win her hand, and there she is being over-dramatic and {i}sleeping{/i}.{p}Emotional health...{p}How stupid."

            #"Talking to the prince doesn't seem to be helping, but poor Amelina. {i}I{/i} wouldn't want to be engaged to this guy."

            hide ezha
            hide prince
            with dissolve

        "Talk to [Lehina_Name]" if not talkedToLehina:
            $ talkedToLehina = True
            show lehina neutral at left
            show ezha neutral at right
            with dissolve

            Fairy_Ezha      "How are they doing, Lehina?"
            Fairy_Lehina    "As good as can be expected. It is hard to maintain hope when you don't even know what caused the problem."

            menu:
                "Yeah, but we'll keep working on it!":
                    Fairy_Ezha      "Yeah, but we'll keep working on it!"
                    show lehina smile2
                    Fairy_Lehina    "I know we will!"
                    
                    if lehina_lives_with_ezha == True:
                        pass
                    
                    else:
                        pass
                        #"[Lehina_Name] smiles warmly at me, and it gives me butterflies."
                        
                    $ lehina += 1

                "They'll just have to be patient.":
                    Fairy_Ezha      "They'll just have to be patient."
                    show lehina nervous
                    Fairy_Lehina    "I suppose..."
                    
                    $ lehina -= 1

            hide ezha
            hide lehina
            with dissolve

        "Talk to sprite" if not talkedToSprite:
            $ talkedToSprite = True
            show suwi neutral at left
            show ezha neutral at right
            with dissolve

            Fairy_Ezha  "Hi, I'm Ezha!"
            Suwi        "I'm Suwi!"
            Fairy_Ezha  "Are you a sprite?"
            Suwi        "I sure am! I'm here to try to help the princess!"
            Fairy_Ezha  "Me too!"
            Suwi        "Yeah! We can do it!!"

            hide ezha
            hide suwi
            with dissolve

        "Go elseware":
            jump sb_ch2_searchHub

        "Discuss evidence with fairies" if ch2_talkedToDuthaha and ch2_foundPotion and ch2_talkToHandmaid and ch2_talkToPrince:
            jump sb_ch2_evidence

    jump sb_ch2_searchTearoom_menu

########################################################################
#                                                   CASTLE - STAFFROOM #
########################################################################
label sb_ch2_searchStaffroom:   # Cook / Handmaid / Oyimahina
    show bg castle_kitchen with dissolve

    # "In the staffroom, the cook is sitting while waiting for water to heat up. The princess' handmaid is here, idly fidgeting with her hands. Oyimahina is talking to another servant in the corner."

label sb_ch2_searchStaffroom_menu:
    menu:
        "Talk to cook" if not talkedToCook:
            $ talkedToCook = True
            show doctor neutral at left
            show ezha neutral at right
            with dissolve

            Fairy_Ezha  "Excuse me, but could you tell me what the princess ate the day before she fell asleep?"
            Cook        "Well, she hadn't eaten much that day...{w} maybe just a biscuit."
            Fairy_Ezha  "That's all? All day?"
            Cook        "Yes, she hasn't had much of an appetite recently.{p}You don't think that could be the cause of her slumber?"
            Fairy_Ezha  "I don't know, I'm still trying to put together the pieces."
            Cook        "Thank you for your work. Please let me know if there's anything else I can help with."

            hide ezha
            hide doctor
            with dissolve

        "Talk to handmaid" if not talkedToHandmaid:
            $ talkedToHandmaid = True
            $ ch2_talkToHandmaid = True
            show doctor neutral at left
            show ezha neutral at right
            with dissolve

            Fairy_Ezha  "Hey, did you notice anything strange about the princess before she fell asleep?"
            Handmaid    "Well, like I told Oyimahina, she had been quite stressed out recently."
            Fairy_Ezha  "For how long?"
            Handmaid    "It's probably been the better part of a year, actually."
            Fairy_Ezha  "Oh?"
            Handmaid    "Yes with her coming of age and all the new responsibilities...{p}She hasn't been handling it well."
            Fairy_Ezha  "Thanks for letting me know. I'm going to investigate more!"
            Handmaid    "Please let me know if there are any updates!"

            hide ezha
            hide doctor
            with dissolve

        "Talk to Oyimahina" if not talkedToOyimahina:
            $ talkedToOyimahina = True
            show oyimahina neutral at left
            show ezha neutral at right
            with dissolve

            Fairy_Ezha      "Find anything out yet, Oyi?"
            show oyimahina sidelook
            
            Fairy_Oyimahina "I have spoken to multiple people. It seems as if the princess had been pretty stressed out lately."
            Fairy_Oyimahina "However, I do not specifically know how that would factor into this condition."

            menu:
                "It could be important!":
                    show ezha thinking
                    
                    Fairy_Ezha      "It could be important!"
                    
                    show oyimahina talking
                    
                    Fairy_Oyimahina "Yes, perhaps..."
                    Fairy_Oyimahina "Maybe the sleep is a result of self-medicating, in some form."
                    
                    Fairy_Ezha      "I guess we should ask around about what she's had access to recently."
                    
                    Fairy_Oyimahina "Great minds think alike."
                    
                    # "Was that a {i}compliment{/i} from Oyimahina?{p}I can feel myself blushing, and I quickly turn away from her to continue my search."
                    $ oyimahina += 1

                "It's probably nothing.":
                    Fairy_Ezha      "Stress isn't {i}that bad{/i}! We should probably look for something else."
                    Fairy_Oyimahina "..."

            hide ezha
            hide oyimahina
            with dissolve

        "Search room" if not searchedStaffroom:
            $ searchedStaffroom = True
            # "I look around the ingredients shelves but there's nothing unusual - all foodstuffs, no magic or poison or ingredients for insomina medicine..."

        "Go elseware":
            jump sb_ch2_searchHub

        "Discuss evidence with fairies" if ch2_talkedToDuthaha and ch2_foundPotion and ch2_talkToHandmaid and ch2_talkToPrince:
            jump sb_ch2_evidence

    jump sb_ch2_searchStaffroom_menu
