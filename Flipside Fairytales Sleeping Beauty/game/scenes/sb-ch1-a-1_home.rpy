label sleeping_beauty:
    call sb_init from _call_sb_init

    #centered "{color=#ffffff} This game is currently a work in progress.  \n Art, music, sound, etc. may change! \n Thanks for trying it out! {/color}"

########################################################################
#                                                                 HOME #
########################################################################
label sb_ch1:

    play music "assets/music/Ozha's_Home.ogg"
    play sound "assets/sounds/Rooster Crowing-SoundBible.com-43612401.ogg"
    $ renpy.pause( 1.0 )

    show bg ezha_house_inner with dissolve
    
    show ezha pjyawn with dissolve

    Fairy_Ezha "*yawn*{p}Morning already?"

    show ezha pajamas

    Fairy_Ezha "Alright, time to start the day!"
    
    hide ezha
    
    window hide    
    show bg chapter_frame with fade
    centered "{color=#ffffff}{size=+20}Chapter 1: The Banquet{/size}{/color}"

    show bg black with fade

    Fairy_Ezha "Feedin' the chickies... feedin' the chickies..."
    
    show bg ezha_house_inner with fade
    show ezha neutral with dissolve

    # Show Ezha in clothes
    
    show ezha thinking

    Fairy_Ezha "Let's see... I need to run to the market, and pick up some new books, and make sure to get some exercise today!"

    show ezha smile

    Fairy_Ezha "Just another laid back day!"

    show ezha neutral

    menu:
        Fairy_Ezha "Now, what should I do first?"

        "The market":
            show ezha smile
            Fairy_Ezha "I should get some fruits at the market today!"
            hide ezha with dissolve
            jump sb_ch1_market

        "The park":
            show ezha smile
            Fairy_Ezha "I should take a relaxing stroll at the park!"
            hide ezha with dissolve
            jump sb_ch1_park

        "The library":
            show ezha smile
            Fairy_Ezha "Today seems like a good day to find a new book to read!"
            hide ezha with dissolve
            jump sb_ch1_library
