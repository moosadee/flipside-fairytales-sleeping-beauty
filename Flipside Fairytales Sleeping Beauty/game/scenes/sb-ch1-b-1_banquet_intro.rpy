########################################################################
#                                                              BANQUET #
########################################################################
label sb_ch1_banquet:

    show bg black
    centered      "Later that day..." with dissolve

    play music "assets/music/Ashton Manor.mp3"
    show bg castle_banquet with fade

    if ( shahina_seeAtMarket == True ):
        show shahinagown neutral at left with dissolve
        Fairy_Shahina       "Uh, girls?"

        show oyimahinagown neutral at right with dissolve

        Fairy_Oyimahina     "What is it, [Shahina_Name]?"

        show lehinagown neutral at center with dissolve

        Fairy_Lehina        "What is it, [Shahina_Name]? What's wrong?"
        Fairy_Shahina       "Uh, well. I was at the market today and, well..."
        Fairy_Oyimahina     "Stop hesitating and just tell us what you want to say."
        Fairy_Shahina       "[Ezha_Name] was there and, uh..."
        Fairy_Oyimahina     "Did you avoid them?"
        Fairy_Shahina       "Uh well, they came up to me to talk, eh..."
        Fairy_Lehina        "Oh! How is [Ezha_Name] doing?"
        Fairy_Oyimahina     "I hope that you did not mention the banquet to them."
        Fairy_Shahina       "Well..."
        Fairy_Oyimahina     "[Shahina_Name]..."
        Fairy_Shahina       "I'm sorry! I didn't... know what to say..."
        Fairy_Oyimahina     "You know that the king and queen do not wish for [Ezha_Name] to be present."
        Fairy_Lehina        "Oh it won't be that bad!"
        Fairy_Oyimahina     "I am sure that the king and queen has not taken the decision to {i}not{/i} invite [Ezha_Name] lightly."
        Fairy_Shahina       "Well... they haven't arrived yet... so maybe they stayed home?"
        Fairy_Oyimahina     "I think that would be unlikely, knowing them."



    elif ( lehina_seeAtLake == True ):
        show lehinagown neutral at left with dissolve
        Fairy_Lehina        "Where is [Ezha_Name]?"

        show shahinagown neutral at center with dissolve

        Fairy_Shahina       "[Ezha_Name] wasn't invited."

        show oyimahinagown neutral at right with dissolve

        Fairy_Oyimahina     "That's correct. The royal family decided that they would prefer for [Ezha_Name] to not be present for the gifting."
        Fairy_Lehina        "What? Why?"
        Fairy_Shahina       "You know..."
        Fairy_Lehina        "I saw them today, though. I let them know about the banquet."
        Fairy_Oyimahina     "That is not ideal."
        Fairy_Lehina        "Come on, you need to loosen up."
        Fairy_Shahina       "I don't know if..."
        Fairy_Oyimahina     "[Ezha_Name] is not welcome here."
        Fairy_Lehina        "Oyima..."
        Fairy_Oyimahina     "They will just cause problems."



    elif ( oyimahina_seeAtLibrary == True ):

        show oyimahinagown neutral at left with dissolve
        show oyimahinagown sidelook2

        Fairy_Oyimahina     "I need to make you aware of something."

        show oyimahinagown sidelook
        show lehinagown neutral at center with dissolve
        show lehinagown nervous3

        Fairy_Lehina        "What's wrong, Oyima?"

        show lehinagown nervous2
        show shahinagown neutral at right with dissolve
        show shahinagown annoyedF2

        Fairy_Shahina       "Bad news??"

        show shahinagown annoyedF
        show oyimahinagown sidelook2

        Fairy_Oyimahina     "[Ezha_Name] found me at the library this morning.{p}I told them about the banquet, and that they're not welcome.{p}"
        
        show oyimahinagown talking
        
        Fairy_Oyimahina     "However, knowing [Ezha_Name], I doubt that they will heed my warning and end up attending the banquet anyway."

        show oyimahinagown sidelook
        show lehinagown nervous3

        Fairy_Lehina        "What? Why aren't they welcome??"
        
        show lehinagown nervous2
        show shahinagown annoyedF2
        
        Fairy_Shahina       "You know how [Ezha_Name] is..."
        
        show oyimahinagown sidelook2
        show shahinagown annoyedF
        
        Fairy_Oyimahina     "Additionally, [Ezha_Name] has not been given enough time to even {i}prepare{/i} a gift."
        Fairy_Oyimahina     "But, I doubt that means that they will simply {i}not do so{/i}, but instead rush to create something without thinking."
        
        show oyimahinagown sidelook
        show shahinagown annoyedF2
        
        Fairy_Shahina       "I mean... that might be OK, right?"
        
        show shahinagown annoyedF
        show lehinagown nervous3
        
        Fairy_Lehina        "Hey, [Ezha_Name]'s gifts aren't so bad..."
        
        show lehinagown nervous2
        show oyimahinagown neutral
        
        Fairy_Oyimahina     "..."


    hide shahinagown with dissolve
    hide oyimahinagown with dissolve
    hide lehinagown with dissolve

    jump sb_ch1_castle_outer
