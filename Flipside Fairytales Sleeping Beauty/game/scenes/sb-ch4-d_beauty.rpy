########################################################################
#                                                       TRIAL - BEAUTY #
########################################################################
label sb_ch3_trial4:
    scene bg black with fade
#   * Beauty        -   her definition of beauty

    # The princess arrives at a dream-castle.     
    # The prince and princess of the castle escort her around and make
    # sure she is cared for after her trying journey.

    # Everyone continually sees her beauty above her self, and mostly feels ignored.
    # This isn't the beauty she chose for herself.
        
    # In preparation for a ball, she is led to a bedroom with a servant to help dress her.
    # You can decide whether you want to impress the prince or the princess,
    # and you can decide your wardrobe, fighting off what the royal family has chosen for you.
    
    # As you enter the ballroom, the announcer asks for your name and title.
    # You can choose "crown prince", "crown princess", or "crown heir" of [location]...
    # And you can type in the princess' new name.
    
    # The princess(/other) dances with the chosen prince/princess,
    # and she awakens from her coma as they kiss...
    
    
    
    Princess        "Wow, what kingdom is this?"
    DreamPrince     "Princess!"
    Princess        "Huh?"
    DreamPrincess   "Oh it's so good to see you, Princess!"
    DreamPrince     "Please come with us!"
    Princess        "Oh?"
    
    # Throne room
    
    DreamPrincess   "I'm sure you're tired from your long journey."
    DreamPrince     "But we're very excited that you're here!"
    DreamPrince     "We shall throw a ball in your honor!"
    DreamPrincess   "...But first, you must rest! A hot bath and a warm bed awaits you."
    Princess        "...What kindgom is this?"
    DreamPrince     "Why, my dear, this is the [Dream_Kingdom] Kingdom."
    Princess        "I am not familiar with--"
    DreamPrincess   "No matter! You may not know {i}us{/i} but we know {i}you{/i} and we're very excited to get to know you!"
    DreamPrince     "And to spoil you with fine gowns and jewels..."
    DreamPrincess   "...To show off your beauty!"
    Princess        "But--"
    DreamPrince     "Now, come with me, dear."
    
    # Somewhere else
    
    DreamPrince     "Our people will be eager to see you."
    DreamPrince     "We are sending out the word: The most beautiful princess in all the land is with us!"
    DreamPrince     "My only request is that you wear these at tomorrow's ball."
    
    # Dream prince gives her jewerly
    
    DreamPrince     "Now, please enjoy the bath we had drawn for you, and relax for now."
    
    Princess        "Wow. This is...{w} Overwhelming."
    
    # Dream prince kisses her hand
    
    # Bedroom
    
    DreamPrincess   "There you are, princess!"
    DreamPrincess   "My, your skin is glowing! How dazzling!"
    DreamPrincess   "I've made sure your bed is set up and I've laid out a dozen gowns for you to choose from."
    DreamPrincess   "Oohh, I'm so excited!"
    
    # The dream princess hugs the princess
    
    DreamPrincess   "You will be absolutely stunning, princess."
    
    # The dream princess kisses her on the cheek??
    
    DreamPrincess   "Now please, have a good rest!"
    
    Princess        "This is all so much to take in."
    
    # Next morning
    
    # Servant knocks on the door
    
    "Servant"       "Princess, I am here to help you get prepared for the ball."
    
    Princess        "Oh. Okay."
    
    "Servant"       "Have you decided on which dress you will choose?"
    
    Princess        "Well... Let me see..."
    Princess        "I don't really like any of these."
    "Servant"       "But! They're only the finest gowns for one of such high stature as yourself!"
    Princess        "But... this isn't {i}my{/i} idea of beauty."
    "Servant"       "Any lady would radiate brilliance in one of these."
    Princess        "Do you have anything else?"
    "Servant"       "Princess?"
    Princess        "Can I see some outfits that... {i}aren't these{/i}?"
    "Servant"       "..."
    "Servant"       "Yes, princess. I shall gather any outfits I can find nearby."

    # Choose your own clothes
    
    "Servant"       "I suppose that will work, princess."
    Princess        "I like this much better."
    "Servant"       "I'm sure our prince and princess will just be happy that you are attending the ball."
    Princess        "Oh yeah, them..."
    
    menu:
        "I hope I impress the prince.":
            $ impressPrince = True
            
        "I hope I impress the princess.":
            $ impressPrincess = True
            
        "I hope I impress both of 'em.":
            $ impressBoth = True
            
        "This is for myself":
            $ impressNone = True

    "Servant"       "Are you ready, princess?"
    Princess        "Yes, I am."
    
    # Entrance to ballroom
    
    "Host"          "Before you enter, I will introduce you."
    "Host"          "What is your title?"
    
    menu:
        "Princess":
            $ title = "Princess"
            $ pronounSet = "She"
            $ childTitle = "daughter"
            
        "Prince":
            $ title = "Prince"
            $ pronounSet = "He"
            $ childTitle = "son"
            
        "The Royal Heir":
            $ title = "Royal Heir"
            $ pronounSet = "They"
            $ childTitle = "child"

    "Host"          "And, what is your name?"
    
    menu:
        "[Princess_FullName]":
            pass
            
        "Something else...":
            $ Princess_Name = renpy.input( "My name is..." )
            $ Princess_Name.strip()
            $ Princess_FullName = Princess_Name + " de Juvela"
            
    Princess    "My name is [Princess_FullName]."
    
    "Host"      "Thank you."
    
    # Show ballroom
    
    "Host"      "Please welcome...{p} [title] [Princess_FullName]!"
    
    # Show the dream prince and dream princess
    
    if title == "Prince" or title == "The Royal Heir":
        DreamPrince     "[title]!{p}I am sorry that I got that wrong earlier."
        DreamPrincess   "Yes, [Princess_Name], thank you for giving us your true name."
        
    
    if impressPrince == True:
        DreamPrince     "Now, [title]... May I have this dance?"
        
    elif impressPrincess == True:
        DreamPrincess   "[title]! May I have this dance?"
        
    elif impressBoth == True:
        DreamPrince     "[title]... May I --"
        DreamPrincess   "[title]! Can I have this dance??"
        DreamPrince     "Sister!!"
        DreamPrincess   "You snooze you lose, brother!"
        
    elif impressNone == True:
        DreamPrince     "Now, [title]... May I have this dance?"
        Princess        "I don't really feel like dancing."
        DreamPrince     "Oh..."
        Princess        "But, do you have any {i}cake{/i} at this banquet?"
        DreamPrincess   "We sure do! Follow me!"
    
    
    # The gift appears and shatters
    
    Fairy_Duthaha "Ahh, [title]. You have chosen who {i}you{/i} are, rather than abiding by what others tell you what you are."
    Princess "It feels... good."
    Princess "But this is just a dream world, isn't it?"
    Fairy_Duthaha "This may be a dream, but no matter where you are, you can still be you."
    
    
    
    # End of the dream        
    
    jump sb_ch4

    
