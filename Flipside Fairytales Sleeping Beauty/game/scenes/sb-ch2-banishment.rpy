########################################################################
#                                                           BANISHMENT #
########################################################################
label sb_ch1_banishment:

    stop music
    show bg black with fade

    centered "The next day..."

    #centered      "The next day..."   with dissolve

    # Perhaps the next week, inserting a bit of dating sim here.

    # The three fairies are over at Ezha's house, helping her pack up with magic.

    # Have a version with boxes everywhere
    show bg ezha_house_moving
    with dissolve

    $ renpy.pause( 1.0 )

    #show lehina neutral at Position(x=0.0) with dissolve
    #show oyimahina neutral at Position(x=0.2) with dissolve
    #show ezha neutral at Position(x=0.4) with dissolve
    #show shahina neutral at right with dissolve

    show lehina sad at left
    show lehina sad2 with dissolve
    Fairy_Lehina    "Where do you want your spellbooks, [Ezha_Name]?"

    show lehina sad
    show ezha depressed at right with dissolve
    show ezha depressed2
    Fairy_Ezha      "Ah, I guess in that box... Those things weigh a ton."

    window hide
    hide ezha
    hide oyimahina
    hide lehina
    show bg chapter_frame with dissolve
    centered "{color=#ffffff}{size=+20}Chapter 2: The Banishment{/size}{/color}"

    show lehina sad at left
    show ezha depressed at right
    show bg ezha_house_moving
    with dissolve

    show oyimahina neutral at center with dissolve
    show oyimahina talking
    Fairy_Oyimahina "You should get rid of some of these things. Do you even read all of these?"

    show oyimahina neutral
    show ezha depressed2
    Fairy_Ezha      "They're {i}reference books{/i}, I don't read them back-to-front, just whatever I need, {i}whenever{/i} I need."

    show ezha depressed
    show oyimahina sidelook
    Fairy_Oyimahina "Yeah but this book on enchanting musical instruments clearly has not yet been opened."

    show oyimahina sidelook2
    show ezha depressed3
    Fairy_Ezha      "..."

    $ renpy.pause( 1.0 )

    hide ezha with dissolve

    show oyimahina sidelook3
    Fairy_Oyimahina "..."

    $ renpy.pause( 1.0 )

    hide oyimahina with dissolve

    # Ezha moves to talk to Shahina on the other side of the room.
    show ezha at Position(x=0.4) with move

    show ezha solemn2
    Fairy_Ezha      "Thanks again for helping me pack, [Shahina_Name]."


    Fairy_Shahina   "Oh... it's fine, [Ezha_Name]."

    # Shahina looks upset
    Fairy_Shahina   "..."
    Fairy_Ezha      "Hey, it's not that bad. Don't look at me like that."

    # Shahina tears up
    Fairy_Shahina   "..."
    Fairy_Ezha      "Don't, you're going to make me start crying, too."

    # Shahina starts crying
    # Ezha starts crying

    Fairy_Shahina   "I'm sorry that you have to leave, [Ezha_Nickname]."
    Fairy_Ezha      "Hey, it's okay. I'm not moving that far away."
    Fairy_Shahina   "I -- {w=1.0} *sniffle* {w=1.0} -- I know...{p}I'm just sorry that you have to go away."
    Fairy_Ezha      "You can come visit me whenever you want, [Shahina_Nickname]!"
    Fairy_Shahina   "Yeah..."

    # fade to black
    # fade back in (room is even more sparse now)

    Fairy_Ezha          "Okay, only a few things left to do!"

    Fairy_Shahina       "I'll work on summoning cleaning sprites."
    Fairy_Lehina        "I'm going to put a prosperity charm on the house for whoever lives here next!"
    Fairy_Oyimahina     "I will go set up a travel spell for the boxes."

    menu:
        Fairy_Ezha      "I'll come help you out, ..."

        "[Lehina_Name]      {image=assets/ui/icon-lehina.png}":
            jump sb_ch1_banishment_lehina

        "[Oyimahina_Name]   {image=assets/ui/icon-oyimahina.png}":
            jump sb_ch1_banishment_oyimahina

        "[Shahina_Name]     {image=assets/ui/icon-shahina.png}":
            jump sb_ch1_banishment_shahina


########################################################################
#                                                       HELPING LEHINA #
########################################################################
label sb_ch1_banishment_lehina:
    $ lehina += 1
    $ lehina_helpMove = True

    # Lehina is outside the house, making a prosperity charm.

    Fairy_Ezha      "Hey, [Lehina_Nickname]. What can I do to help?"
    Fairy_Lehina    "Help me scatter these petals around the perimeter of your house."

    # fade out
    # fade in

    Fairy_Lehina    "Thanks, [Ezha_Name]!"
    Fairy_Ezha      "No, thanks to you! What else do you need?"
    Fairy_Lehina    "Now while I'm reciting the spell, we should think about the things we are thankful for."
    Fairy_Ezha      "Just, like, anything?"
    Fairy_Lehina    "Yeah, it can be anything. We just need to send thoughts of gratitude to the house."

    menu:
        Fairy_Ezha  "I'm thankful for..."

        "[Lehina_Name]!":
            call sb_ch1_banishment_lehina_appreciate_lehina from _call_sb_ch1_banishment_lehina_appreciate_lehina

        "This house, which has taken care of me over the years!":
            call sb_ch1_banishment_lehina_appreciate_house from _call_sb_ch1_banishment_lehina_appreciate_house

        "I don't feel thankful for anything, right now.":
            call sb_ch1_banishment_lehina_appreciate_nothing from _call_sb_ch1_banishment_lehina_appreciate_nothing

    jump sb_ch1_banishment_endofday


label sb_ch1_banishment_lehina_appreciate_lehina:
    $ lehina += 1
    $ lehina_appreciateLehina = True

    Fairy_Ezha      "I appreciate my friend [Lehina_Name]! Thanks to her for always being there for me."
    Fairy_Lehina    "Aww, that's really sweet, [Ezha_Nickname]. Thank you!"
    Fairy_Lehina    "And I appreciate {i}my{/i} friend, [Ezha_Name]! My effervescent, sincere, energetic friend."

    # Smile
    $ renpy.pause( 1.0 )
    Fairy_Ezha      "Ahh!{p}I had been feeling down about moving, but I feel really happy now."
    Fairy_Lehina    "We'll still see each other. You just don't have to work now."
    Fairy_Ezha      "Yeah, but I'll still miss you all!"

    return

label sb_ch1_banishment_lehina_appreciate_house:
    $ lehina += 1
    $ lehina_appreciateHouse = True

    Fairy_Ezha      "I appreciate this house, for giving me shelter and taking care of me all these years."
    Fairy_Lehina    "I also appreciate this house, for taking care of my dear friend [Ezha_Name]."

    # Looking sad
    Fairy_Ezha      "It's been a good house."
    Fairy_Lehina    "I know. And it will be a good house for its future residents."
    Fairy_Lehina    "And we'll come visit you at your new house, too. You'll have a new home but the same friends."
    Fairy_Ezha      "Yeah..."

    # Ezha is tearing up
    Fairy_Ezha      "*sigh*"

    # Lehina hugs Ezha
    Fairy_Lehina    "It's alright, [Ezha_Nickname]."
    Fairy_Ezha      "Thanks...{w=1.0} for being here...{w=1.0} [Lehina_Name]"

    Fairy_Lehina    "Anytime."

    # Lehina kisses you on the cheek
    Fairy_Lehina    "*peck*"

    menu:
        "Kiss [Lehina_Name]":
            call sb_ch1_banishment_lehina_appreciate_house_kiss from _call_sb_ch1_banishment_lehina_appreciate_house_kiss

        "Squeeze-hug [Lehina_Name]":
            call sb_ch1_banishment_lehina_appreciate_house_hug from _call_sb_ch1_banishment_lehina_appreciate_house_hug

        "I need some time alone":
            call sb_ch1_banishment_lehina_appreciate_house_alone from _call_sb_ch1_banishment_lehina_appreciate_house_alone

    return

label sb_ch1_banishment_lehina_appreciate_nothing:
    $ lehina -= 1
    $ lehina_appreciateNothing = True

    Fairy_Ezha      "I don't feel very appreciative of anything, right now. I've been banished, I have to move..."
    # Sad look
    Fairy_Lehina    "There's nothing that you feel grateful for right now?"
    Fairy_Ezha      "Not really."

    $ renpy.pause( 1.0 )

    Fairy_Lehina    "Alright... I'll complete the charm myself, then."

    return


label sb_ch1_banishment_lehina_appreciate_house_kiss:
    $ lehina += 4
    $ lehina_kiss = True

    # Kiss image

    Fairy_Ezha      "I'm still going to be further away from you than I would prefer."
    Fairy_Lehina    "Well...{p}Maybe I'll leave the kingdom, too."
    Fairy_Ezha      "What? No!{p}Don't leave just because of me..."
    Fairy_Lehina    "I don't intend to stay in the kingdom {i}forever{/i}.{p}Maybe I'll leave sooner rather than later..."
    Fairy_Lehina    "Since I'll feel more at home close to you."
    Fairy_Ezha      "[Lehina_Name]..."

    return

label sb_ch1_banishment_lehina_appreciate_house_hug:
    $ lehina += 2
    $ lehina_hug = True

    # Back to hug graphic

    Fairy_Lehina    "Everything will be alright, [Ezha_Nickname]"
    Fairy_Ezha      "I know. I have you."

    return

label sb_ch1_banishment_lehina_appreciate_house_alone:
    $ lehina_alone = True

    # Not hugging anymore

    Fairy_Ezha      "I...{w=1.0} I need some time alone.{p}I have a lot to process."
    Fairy_Lehina    "I understand."

    return


########################################################################
#                                                   HELPING OYIMHAHINA #
########################################################################
label sb_ch1_banishment_oyimahina:
    $ oyimahina += 1
    $ oyimahina_helpMove = True

    # Outside of Ezha's house, with a pile of boxes. Oyimahina will draw
    # a sigil on the ground to teleport the boxes to their new destination.

    Fairy_Ezha      "Hey, [Oyimahina_Nickname]. What can I do to help?"
    Fairy_Oyimahina "I can take care of everything."
    Fairy_Ezha      "I know you {i}can{/i} do it all, but I should help out.{p}What can I do?"
    Fairy_Oyimahina "*sigh*{w=1.0}Alright, [Ezha_Name]. You can, hmm... draw the sigil, if you prefer."
    Fairy_Ezha      "Got it!"

    # Sigil under the boxes

    Fairy_Ezha      "I've never done a teleportation spell before. This is pretty neat!"
    Fairy_Oyimahina "It takes a lot of preparation and concentration to do a successful teleportation, even just for inanimate objects."

    menu:
        "It's impressive":
            call sb_ch1_banishment_oyimahina_impressive from _call_sb_ch1_banishment_oyimahina_impressive

        "What's the worst that can happen??":
            call sb_ch1_banishment_oyimahina_whatcanhappen from _call_sb_ch1_banishment_oyimahina_whatcanhappen

        "Concentration, eh? Perhaps I should begin yodeling!":
            call sb_ch1_banishment_oyimahina_yodeling from _call_sb_ch1_banishment_oyimahina_yodeling


    jump sb_ch1_banishment_endofday


label sb_ch1_banishment_oyimahina_impressive:
    $ oyimahina += 1
    $ oyimahina_calledImpressive = True

    Fairy_Ezha      "These spells are so difficult. I'm really impressed by you, [Oyimahina_Nickname]."

    # Oyi blushes a little
    Fairy_Oyimahina "It just requires study and practice, just as with any other skill."
    Fairy_Ezha      "Yeah, but you took the time to master it! I really admire that."

    # More blushing
    $ renpy.pause( 1.0 )
    Fairy_Oyimahina "..."
    Fairy_Oyimahina "I might not be able to finish this spell right now."
    Fairy_Oyimahina "Perhaps you should go help out one of the others, so that I can focus better."
    Fairy_Ezha      "Oh, sorry!{p}I can be quiet so you can work."
    Fairy_Oyimahina "I, uh...{p}I need some time.{w=1.0} To regain my focus."
    Fairy_Ezha      "Okay, [Oyimahina_Nickname]. I'll leave you alone for now!"

    return

label sb_ch1_banishment_oyimahina_whatcanhappen:
    Fairy_Ezha      "Well what's the worst that can happen??"
    Fairy_Oyimahina "Well, for your boxes, if I mess this up, then your possessions may end up strewn across the land instead of in front of your new dwelling."

    menu:
        "Oh, okay. I'll be quiet...":
            call sb_ch1_banishment_oyimahina_whatcanhappen_quiet from _call_sb_ch1_banishment_oyimahina_whatcanhappen_quiet

        "I'll take that chance just to be able to talk to you!":
            call sb_ch1_banishment_oyimahina_whatcanhappen_talk from _call_sb_ch1_banishment_oyimahina_whatcanhappen_talk

    return

label sb_ch1_banishment_oyimahina_yodeling:
    $ oyimahina -= 1
    $ oyimahina_yodelingWhenCasting = True

    Fairy_Ezha      "YODELYODELYODA-LAY-HEE-HOOOO!"

    # Oyimahina looks pissed

    Fairy_Oyimahina "[Ezha_Name], I would prefer to not have you here while I am trying to focus."
    Fairy_Ezha      "Aww, [Oyimahina_Nickname], I was just trying to tease you!"
    Fairy_Oyimahina "I do not find it very entertaining."

    return

label sb_ch1_banishment_oyimahina_whatcanhappen_quiet:
    $ oyimahina += 1
    $ oyimahina_quietWhenCasting = True

    Fairy_Ezha      "Oh, ok. I'll be quiet..."
    Fairy_Oyimahina "Thank you."

    # Oyi does the spell

    Fairy_Oyimahina "That should take care of it."
    Fairy_Oyimahina "Assuming that nothing went wrong, your possessions should already be at your new home."
    Fairy_Ezha      "Thanks so much, [Oyimahina_Nickname]!"

    return

label sb_ch1_banishment_oyimahina_whatcanhappen_talk:
    $ oyimahina += 2
    $ oyimahina_wantToTalk = True

    Fairy_Ezha      "I'll take that chance just to be able to talk to you!"

    # Oyi blushes a little and looks surprised.

    $ renpy.pause( 1.0 )

    Fairy_Oyimahina "..."

    # Looks more collected now
    Fairy_Oyimahina "I am not very good at small talk."
    Fairy_Ezha      "That's okay! I just like talking to you."
    Fairy_Oyimahina "..."
    Fairy_Ezha      "And spending time with you!{p}That's why I'm out here!"

    # Oyimahina is blushing again
    $ renpy.pause( 1.0 )
    Fairy_Oyimahina "I, uh...{p}I cannot think of much to talk about right now, [Ezha_Name]."
    Fairy_Ezha      "Well, we can just sit out here and wait for the others to finish up!"

    # Oyimahina smiles, a little
    Fairy_Oyimahina "...{p}Okay."

    return

########################################################################
#                                                      HELPING SHAHINA #
########################################################################
label sb_ch1_banishment_shahina:
    $ shahina += 1
    $ shahina_helpMove = True

    hide lehina
    hide ezha
    show shahina neutral at right
    show bg ezha_kitchen_moving with fade

    $ renpy.pause( 1.0 )

    show ezha neutral at left with dissolve

    # Shahina is in the kitchen summoning cleaning sprites.

    Fairy_Ezha      "Hey, [Shahina_Nickname]. What can I do to help?"
    Fairy_Shahina   "Oh!{w=1.0}Hmm...{p}Well, I've already summoned the sprites."
    Fairy_Shahina   "They'll take care of most of it. We just need to come up with a reward to offer the sprites once they're finished."
    Fairy_Ezha      "What do the sprites like?"
    Fairy_Shahina   "Well, it depends..."

    menu:
        Fairy_Ezha  "How about..."
        "Cute little sprite clothes?":
            call sb_ch1_banishment_shahina_clothes from _call_sb_ch1_banishment_shahina_clothes

        "Lunch?":
            call sb_ch1_banishment_shahina_lunch from _call_sb_ch1_banishment_shahina_lunch

        "Nothing? They're just sprites.":
            call sb_ch1_banishment_shahina_nothing from _call_sb_ch1_banishment_shahina_nothing

    jump sb_ch1_banishment_endofday

label sb_ch1_banishment_shahina_lunch:
    $ shahina += 1
    $ shahina_spritesLunch = True

    Fairy_Ezha      "What do they like to eat? Maybe I can fix something for them!"
    Fairy_Shahina   "Yes, it's only right to feed those who help you move."
    Fairy_Ezha      "Yep!"
    $ renpy.pause( 1.0 )
    # Nervous
    Fairy_Ezha      "Oh, you know that I'm treating you all to lunch afterwards, right?"
    Fairy_Shahina   "I know! But let's feed the sprites first!"

    # They make tiny sandwiches

    Fairy_Shahina   "Thanks for helping me make tiny sandwiches, [Ezha_Nickname]!"
    Fairy_Ezha      "No, thanks to you and the sprites for your help!"

    return

label sb_ch1_banishment_shahina_clothes:
    $ shahina += 1
    $ shahina_spritesClothes = True

    Fairy_Ezha      "Maybe we can make them some tiny sprite outfits! Would they like that?"
    Fairy_Shahina   "I'm sure they would like that! Did you already pack a sewing machine?"
    Fairy_Ezha      "Sewing machine?? I just enchant my needles!"
    Fairy_Shahina   "That sounds interesting! Will you show me how?"

    menu:
        "Of course!":
            call sb_ch1_banishment_shahina_clothes_yes from _call_sb_ch1_banishment_shahina_clothes_yes

        "Sorry, it's a family secret!":
            call sb_ch1_banishment_shahina_clothes_no from _call_sb_ch1_banishment_shahina_clothes_no

    return

label sb_ch1_banishment_shahina_nothing:
    $ shahina -= 1
    $ shahina_spritesNothing = True

    Fairy_Ezha      "Nothing? They're just sprites."
    Fairy_Shahina   "They may not be fairies but they still deserve our respect and appreciation."
    Fairy_Ezha      "Eh?"
    Fairy_Shahina   "..."
    Fairy_Shahina   "It's alright, [Ezha_Name]. I'll take care of the sprites. You can go do something else."

    return

label sb_ch1_banishment_shahina_clothes_no:
    $ shahina += 1
    $ shahina_spritesClothesSecret = True

    Fairy_Ezha      "Oh, I'm sorry [Shahina_Name]. It's a still of my grandmother, and I'd prefer to keep it a family skill!"
    Fairy_Shahina   "Ohh...{w=1.0} Alright."
    Fairy_Shahina   "I'm a little disappointed, though."

    # Ezha flirty
    Fairy_Ezha      "Maybe one day!"

    # Slight blush
    Fairy_Shahina   "One day...?"

    # fade to black
    # fade in, sprites are wearing the clothes

    Fairy_Shahina   "The sprites seem so happy!"
    Fairy_Ezha      "And they're looking sharp!"
    Fairy_Shahina   "Thank you for all your help!"
    Fairy_Ezha      "Yes, thank you, lil' guys!"

    return

label sb_ch1_banishment_shahina_clothes_yes:
    $ shahina += 2
    $ shahina_spritesClothesShow = True

    Fairy_Ezha      "You know, my grandmother always wanted to keep this skill just to our family..."
    Fairy_Ezha      "But you're as close to me as any sister, [Shahina_Nickname]!"
    Fairy_Ezha      "Just don't tell anyone I showed you this."
    Fairy_Shahina   "Oh, wow! Alright!"

    # fade to black
    # fade in, sprites are wearing the clothes

    Fairy_Shahina   "The sprites seem so happy!"
    Fairy_Ezha      "And they're doubly cute now!"
    Fairy_Shahina   "Thank you for all your help!"
    Fairy_Ezha      "Yes, thank you, lil' guys!"

    return

########################################################################
#                                                       END OF THE DAY #
########################################################################
label sb_ch1_banishment_endofday:

    if lehina_kiss:
        $ lehina_lives_with_ezha = True

    hide ezha
    show bg black with fade
    $ renpy.pause( 1.0 )

    jump sb_ch2
