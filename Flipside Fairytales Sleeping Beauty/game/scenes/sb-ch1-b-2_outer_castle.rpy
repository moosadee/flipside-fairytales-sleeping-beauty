########################################################################
#                                                         OUTER CASTLE #
########################################################################
label sb_ch1_castle_outer:

    show bg black with fade
    play music "assets/music/The Path of the Goblin King.mp3"
    show bg castle_outer with fade

    show guard neutral2 at left with dissolve

    show ezhagown neutral at right with moveinright

    # show guard

    show guard neutral

    Guard           "Name?"

    show guard neutral2
    show ezhagown proud

    Fairy_Ezha      "[Ezha_FullName]"

    show guard read

    Guard           "..."

    show guard neutral

    Guard           "You're not permitted to enter."

    show guard neutral2
    show ezhagown thinking

    Fairy_Ezha      "What? But I'm a fairy."

    show guard neutral

    Guard           "You're not on the invite list."

    show guard read2

    Guard           "What's more, you're explicitly on the \"Do not let in\" list."

    show guard neutral2
    show ezhagown skeptical

    Fairy_Ezha      "That's not right... All fairies have the right to bequeath gifts to the royal family!"

    show guard neutral

    Guard           "The royal family has specifically requested..."

    show guard worry

    # Shake the screen

    show ezhagown mad with vpunch
    Fairy_Ezha      "LOOK HERE MISTER."

    show ezhagown yell with vpunch
    Fairy_Ezha      "DO YOU WANT THE BABY TO BE DEVOURED BY DEMONS?"

    show guard worry

    Guard           "???"

    show ezhagown yell2 with vpunch
    Fairy_Ezha      "DRAGONS?"

    show guard worry2b

    Guard           "No..."
    
    show guard worry2
    show ezhagown yell3 with vpunch
    Fairy_Ezha      "GIGANTIC SPIDERS?"

    show guard worry3b

    Guard           "What?!"
    
    show guard worry3
    show ezhagown mad with vpunch
    Fairy_Ezha      "GIGANTIC SPIDERS WILL ATTACK THE CASTLE AND EAT YOU IF YOU DON'T LET ME IN!"
    Fairy_Ezha      "THE ROYAL FAMILY NEEDS OUR PROTECTION!"

    show guard worry3b
    Guard           "Bu..."

    show guard worry3
    show ezhagown yell
    Fairy_Ezha      "OUR BLESSINGS FEND OFF THE SPIDERS."

    show guard worry4

    show guard worry4b
    Guard           "Ahh..."

    show guard worry4
    show ezhagown mad
    Fairy_Ezha      "SO IF YOU DON'T LET ME IN..."

    show guard worry4b
    Guard           "I can't..."

    show guard worry4
    show ezhagown yell3
    Fairy_Ezha      "SPIDERS WILL INVADE FROM EVERYWHERE!"

    show guard worry3

    show ezhagown mad
    Fairy_Ezha      "Let THAT sink in!"
    
    show ezhagown spell
    Fairy_Ezha      "{size=-10}{color=#00ffff}(medóhin dathimezhubeth!){/color}{/size}"
    # animation / sound effect
    
    play sound "assets/sounds/249819__spookymodem__magic-smite.ogg"

    $ renpy.pause( 0.2 )

    # spider graphic

    show spiders
    show ezhagown spiders with hpunch
    
    $ renpy.pause( 0.8 )

    Fairy_Ezha "Oh no, here they come!!"

    show guard spiders with hpunch

    Guard "AAGGHHH!!"
    
    show ezhagown innocent
    Fairy_Ezha      "Guess it's up to me to save the daaay~!"
    $ renpy.pause( 1.0 )

    show ezhagown yell3
    Fairy_Ezha      "{size=+10}{color=#00ffff}(meradóhin dathimezhubeth!){/color}{/size}"
    
    
    # animation / sound effect
    
    show ezhagown magic with vpunch
    play sound "assets/sounds/216089__richerlandtv__magic.ogg"
    
    show magic with hpunch
    
    $ renpy.pause( 0.5 )

    hide spiders
    hide magic
    show magic2 with hpunch
    show guard petrified
    

    # Explosions
    
    $ renpy.pause( 0.5 )
    hide magic2
    show ezhagown yell3

    Fairy_Ezha "BEGONE, SPIDER MENACE!"
    
    $ renpy.pause( 0.5 )
    
    show ezhagown proud
    Fairy_Ezha      "See? It is up to us fairies to keep the castle safe."

    show ezhagown hero
    
    Fairy_Ezha      "Well, it looks like you're speechless."
    
    show ezhagown proud
    Fairy_Ezha      "Don't worry, you don't have to thank me for saving the kingdom from spiders."
    
    show ezhagown innocent
    Fairy_Ezha      "Now, if you'll excuse me..."

    jump sb_ch1_b3_banquet
