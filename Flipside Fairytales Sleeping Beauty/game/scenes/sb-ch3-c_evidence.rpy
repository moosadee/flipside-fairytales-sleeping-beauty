########################################################################
#                                                             EVIDENCE #
########################################################################
label sb_ch2_evidence:
    # "I think it's time to talk to the other fairies."
    show bg castle_bedroom with dissolve

    show shahina neutral at Position(xpos=0.1)
    show lehina neutral at Position(xpos=0.35)
    show ezha neutral at Position(xpos=0.65)
    show oyimahina neutral at Position(xpos=0.9)
    with dissolve

    Fairy_Lehina    "What have you found, [Ezha_Name]?"

    Fairy_Ezha      "Well first off, there's that other fairy that came with Prince [Prince_Name] who seemed really avoidant."
    Fairy_Shahina   "[Duthaha_Name]?"
    Fairy_Ezha      "Yeah, her!{p}She wouldn't really answer my questions and seemed to be planning something..."
    Fairy_Ezha      "And then there's Prince [Prince_Name], who seems like a jerk!"
    Fairy_Lehina    "The princess' fiancé?"
    Fairy_Ezha      "Yeah, no wonder the princess was feeling stressed!"
    Fairy_Oyimahina "Yeah, the princess' handmaid told me that she had been stressed since the engagement."
    Fairy_Ezha      "And the prince was SO dismissive of her stress when I brought it up!"
    Fairy_Shahina   "Do you think {i}he{/i} did something to the princess?"
    Fairy_Ezha      "I don't know! But I also found this bottle."
    Fairy_Oyimahina "Let me see that..."
    Fairy_Lehina    "The work of another fairy?"
    Fairy_Shahina   "What does it look like, [Oyimahina_Name]?"
    Fairy_Oyimahina "I do not recognize it.{p}I have not seen any potions before that look or smell like it."

    "?"             "It's a potion to fix what you broke."

    hide shahina
    hide lehina
    hide ezha
    hide oyimahina
    with dissolve
    show duthaha neutral at Position(xpos=0.7) with moveinright

    Fairy_Duthaha   "I made it."

    show ezha mad at Position(xpos=0.3)
    
    Fairy_Ezha      "YOU poisoned the princess??"
    Fairy_Duthaha   "It {i}did{/i} cause their slumber, though it {i}isn't{/i} poison."
    
    hide ezha
    show oyimahina sidelook at Position(xpos=0.3)
    Fairy_Oyimahina "Why would a fairy trusted by a royal family do this?"
    hide oyimahina
    show shahina nervous2 at Position(xpos=0.3)
    
    Fairy_Shahina   "Is this an assassination attempt?"
    Fairy_Duthaha   "No, not at all. It's a potion to help them."
    
    hide shahina
    show lehina unsure at Position(xpos=0.3)
    Fairy_Lehina    "How is this possibly going to help the princess??"
    hide lehina
    
    #"Duthaha's cool smile turns into a cold frown. The rest of us are at a loss for words."

    # Show flashback scene

    Fairy_Duthaha   "I gave it to her after the engagement ceremony in [Arjento_Kingdom]. She was quite distressed, and I was the only one who took the time to listen to her."
    show lehina unsure at Position(xpos=0.3)
    Fairy_Lehina    "She did seem somewhat solemn during the engagement."

    Fairy_Duthaha   "She does not want to marry prince [Prince_Name]."
    hide lehina
    show oyimahina sidelook at Position(xpos=0.3)
    Fairy_Oyimahina "Then why didn't she tell her parents that?"
    Fairy_Duthaha   "Because her parents want this marriage and she is {b}obedient{/b}."

    Fairy_Duthaha   "Every time the prince and princess spent time together prior to the engagement, he would make her feel terrible about herself."
    hide oyimahina
    show ezha mad at Position(xpos=0.3)
    Fairy_Ezha      "I can believe that."
    hide ezha
    show lehina unsure at Position(xpos=0.3)    
    Fairy_Lehina    "Well why didn't she stand up for herself?"
    Fairy_Duthaha   "Because she's {b}kind{/b}."
    hide lehina
    show shahina nervous2 at Position(xpos=0.3)    
    Fairy_Shahina   "Well if they obviously don't like each other, why does the prince want to marry her?"
    Fairy_Duthaha   "Because she's {b}beautiful{/b}."

    hide shahina
    # "The other fairies are tense now - Duthaha is putting them in their place for giving the princess such useless gifts, I can see that now."

    show ezha mad at Position(xpos=0.3)
    Fairy_Ezha      "Geeze, okay! So she wasn't happy, we could have worked around that. How did she ever think it was okay to just take some weird potion from another kingdom's fairy?!"
    Fairy_Duthaha   "Because she was desperate to escape her situation, and she is {b}brave{/b} enough to confront danger."

    show ezha stunned2
    #"I blink...{w} ouch.{w} Okay, she's calling {i}all of us{/i} out."

    show ezha stunned
    Fairy_Ezha      "Hey now, it's not like bravery is a {i}bad{/i} trait...{p}I mean obedience?{w} She didn't even have a choice in whether or not to marry the prince."

    # Duthaha looks angry

    Fairy_Duthaha   "These traits... She didn't have a choice in having any of them."
    Fairy_Ezha      "But it's customary to--"
    Fairy_Duthaha   "Just because it's a custom doesn't mean it's right.{p}Who are {i}you{/i} to dictate {i}who she will be{/i} while she's still a baby?!"

    show ezha stunned2
    #"There's a thick silence in the room. I glance at the floor, unsure of how to respond."

    hide ezha
    show shahina nervous2 at Position(xpos=0.3)     
    Fairy_Shahina   "So...{w} the slumber..."
    Fairy_Duthaha   "It's the only way to undo your {i}gifts{/i}.{p}She's stuck inside her mind until she confronts those traits and defeats them."
    hide shahina
    show lehina unsure at Position(xpos=0.3)   
    Fairy_Lehina    "How can we help?"
    Fairy_Duthaha   "{i}You cannot.{/i}{p}She has to grow on her own."
    $ renpy.pause( 1.0 )

    hide lehina
    show oyimahina sidelook at Position(xpos=0.3)
    Fairy_Oyimahina     "So...{w} What do {i}we{/i} do?"

    Fairy_Duthaha   "All you can do is wait."

    stop music

    hide ezha
    hide oyimahina
    hide shahina
    hide lehina
    hide duthaha
    with dissolve

    jump sb_ch3




