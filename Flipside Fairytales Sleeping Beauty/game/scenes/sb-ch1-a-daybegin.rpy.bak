label sleeping_beauty:
    call sb_init from _call_sb_init

    #centered "{color=#ffffff} This game is currently a work in progress.  \n Art, music, sound, etc. may change! \n Thanks for trying it out! {/color}"

########################################################################
#                                                                 HOME #
########################################################################
label sb_ch1:

    play music "assets/music/Somewhere Sunny.mp3"
    play sound "assets/sounds/Rooster Crowing-SoundBible.com-43612401.mp3"
    $ renpy.pause( 1.0 )

    show bg ezha_house_inner with dissolve
    
    show ezha pjyawn with dissolve

    Fairy_Ezha "*yawn*{p}Morning already?"

    show ezha pajamas

    Fairy_Ezha "Alright, time to start the day!"
    
    hide ezha
    window hide
    show bg chapter_frame with fade

    centered "{color=#ffffff}{size=+20}Chapter 1: The Banquet{/size}{/color}"

    show bg black with fade

    Fairy_Ezha "Feedin' the chickies... feedin' the chickies..."
    
    show bg ezha_house_inner with fade
    show ezha neutral with dissolve

    # Show Ezha in clothes
    
    show ezha thinking

    Fairy_Ezha "Let's see... I need to run to the market, and pick up some new books, and make sure to get some exercise today!"

    show ezha smile

    Fairy_Ezha "Just another laid back day!"

    show ezha neutral

    menu:
        Fairy_Ezha "Now, what should I do first?"

        "The market":
            show ezha smile
            Fairy_Ezha "I should get some fruits at the market today!"
            hide ezha with dissolve
            jump sb_ch1_market

        "The park":
            show ezha smile
            Fairy_Ezha "I should take a relaxing stroll at the park!"
            hide ezha with dissolve
            jump sb_ch1_park

        "The library":
            show ezha smile
            Fairy_Ezha "Today seems like a good day to find a new book to read!"
            hide ezha with dissolve
            jump sb_ch1_library

########################################################################
#                                                               MARKET #
########################################################################
label sb_ch1_market:
    $ shahina_seeAtMarket = True
    $ shahina += 1

    play music "assets/music/Enchanted Valley.mp3"
    show bg village_market with dissolve
    show ezha bewildered with dissolve

    Fairy_Ezha "Woah! So many people! I hope I can find what I need!"

    # Make it seem like Ezha is walking by moving the whole scene over
    show shopkeeper neutral at left with moveinleft

    show ezha nervous

    Fairy_Ezha "Excuse me ma'am, do you have any fresh apples?"
    
    show ezha nervous2
    show shopkeeper talking
    
    Shopkeeper "Sorry ma'am, we're out of our stock."

    show shopkeeper neutral
    show ezha bewildered

    Fairy_Ezha "Really? Why is that?"

    show ezha bewildered2
    show shopkeeper talking
    
    Shopkeeper "The royal family is having a banquet for their new daughter."

    show shopkeeper neutral
    show ezha thinking2

    Fairy_Ezha "The king and queen had a child?{p}How did I not know this??"
    
    show ezha thinking
    
    hide shopkeeper with moveoutleft

    show ezha thinking2
    Fairy_Ezha "I guess I'll check some other stalls..."
    
    hide ezha with moveoutleft
    
    $ renpy.pause( 1.0 )
    
    show ezha bewildered with moveinleft
    
    Fairy_Ezha "Man, there's nothing here. Everything's been sold!"
    show ezha bewildered2
    Fairy_Ezha "..."
    
    $ renpy.pause( 1.0 )
    
    show ezha wave
    Fairy_Ezha "OH HEY! I see my buddy over there!"

    Fairy_Ezha "Shahina!"

    # sliding perspective trick again
    show shahina side at left with moveinleft

    Fairy_Shahina "Huh?"

    show ezha smile
    show shahina uncomfy
    Fairy_Ezha "Hey [Shahina_Nickname]!{p}Wow, such a crowd today, huh?"

    show ezha neutral
    show shahina nervous3

    Fairy_Shahina "Yes... such a big crowd!" # look nervous

    show shahina nervous4
    show ezha thinking2

    Fairy_Ezha "Are you going to the banquet tonight?"

    show shahina nervous
    show ezha thinking

    show shahina nervous3
    Fairy_Shahina "Oh!" 
    show shahina nervous
    Fairy_Shahina "Well..."
    show shahina nervous2
    Fairy_Shahina "Yes, I am..." # shocked

    show ezha thinking2

    Fairy_Ezha "When did the king and queen have a child?"

    show shahina nervous2

    Fairy_Shahina "*squirms*"

    show shahina nervous3

    Fairy_Shahina "Erm, not too long ago... They had been trying for a while now."

    show shahina nervous4
    show ezha smile

    Fairy_Ezha "Great! I'm so happy to hear! When do we get together to bless the child with gifts?"

    show shahina annoyed
    
    Fairy_Shahina "...mrph..."
    
    show ezha nervous
    
    Fairy_Ezha "Hey... are you okay?"
    
    Fairy_Ezha "..."
    
    $ renpy.pause( 1.0 )
    
    play sound "assets/sounds/460042__dbbaker01__piano-shock-impact.wav"

    # Shock sound

    show ezha bewildered with hpunch

    Fairy_Ezha "...Is that tonight? At the banquet?"

    show shahina nervous2
    show ezha stunned2

    Fairy_Shahina "Well... You see..."

    show ezha stunned

    Fairy_Ezha "Nobody told me! I haven't had time to prepare!"

    show shahina annoyed2
    show ezha stunned2
    
    Fairy_Shahina "Ezh... Uh..."
    
    show shahina annoyed
    show ezha thinking2
    
    Fairy_Ezha "What is it, [Shahina_Nickname]?"

    show ezha thinking
    
    show shahina nervous2
    
    $ renpy.pause( 5.0 )
    
    show shahina nervous3
    
    Fairy_Shahina "Yeah...{w=1.0} You don't have a lot of time to prepare..."
    Fairy_Shahina "I mean... maybe it's too late to make a gift, you know?{p}Maybe you should just come and enjoy the food."
    
    show shahina nervous4
    show ezha proud
    
    Fairy_Ezha "Nonsense! As a fairy of the kingdom, it is my duty to give the royal child a gift!"
    
    show shahina nervous2
    Fairy_Shahina "I--"

    show ezha smile

    Fairy_Ezha "Well I had better get going home to prepare something! I will see you tonight!"

    show ezha neutral
    show shahina nervous

    Fairy_Shahina "OK... yeah..."
    
    show shahina nervous2
    hide ezha with dissolve
    
    $ renpy.pause( 1.0 )
    
    hide shahina with dissolve
    show bg black with dissolve

    Fairy_Ezha "Wow, I've gotta work fast if I'm going to have a gift ready for tonight!"
    Fairy_Ezha "Time to run home!"

    $ shahina += 1
    
    jump sb_ch1_home

########################################################################
#                                                                 PARK #
########################################################################
label sb_ch1_park:
    $ lehina_seeAtLake = True
    $ lehina += 1
    
    play music "assets/music/Windswept.mp3"
    show bg village_park with dissolve
    show ezha neutral with dissolve
    
    Fairy_Ezha "Ahh, what a beautiful day!{p}The sun is shining, the light breeze feels good, and the lake is shimmering!"
    Fairy_Ezha "Oh look, and there are even some tadpoles swimming in the lake! And--"

    show ezha bewildered with hpunch
    Fairy_Ezha "OOF!"
    
    show ezha bewildered2
    show lehina smile2 at left with dissolve
    
    Fairy_Lehina "Oh no, Ezha! Are you okay?"

    show lehina smile
    show ezha smile
    
    Fairy_Ezha "Oh, Lehina! Hi!!"

    show lehina smile2
    show ezha nervous
    
    Fairy_Ezha "Yeah, I'm fine.{p}Are you okay?"
    
    show ezha nervous2
    show lehina talking
    
    Fairy_Lehina "Yep! Sorry about that, I was just distracted."

    show lehina neutral
    show ezha smile
    Fairy_Ezha "Oh it's probably my fault, I was watching the pond."

    show lehina smile2
    Fairy_Lehina "Oh, don't tell me that you're going to give the child one of those slimey things as your gift."

    show ezha nervous
    Fairy_Ezha "Huh?{w} Child?{w} Gift?"

    show lehina nervous
    Fairy_Lehina "The banquet is tonight, you know?"
    
    show lehina nervous2
    show ezha bewildered
    
    Fairy_Ezha "Banquet??"

    show lehina nervous
    show ezha bewildered2
    Fairy_Lehina "The... King and queen had a daughter?"

    show lehina nervous2 
    show ezha stunned with hpunch
    
    Fairy_Ezha "They had a kid?! Finally??"

    show ezha nervous2
    show lehina smile
    
    Fairy_Lehina "Yeah! They have a newborn baby!"
    
    show lehina smile2
    show ezha charmed
    
    Fairy_Ezha "Woah! Congratulations to them! They finally have their first child!"

    show ezha charmed2
    show lehina smile
    
    Fairy_Lehina "Yes and they're very excited."

    show lehina unsure
    Fairy_Lehina "..."

    show lehina unsure2
    Fairy_Lehina "Oh but the gifting banquet is tonight. Did you not get an invitation?"

    show lehina unsure3
    show ezha stunned
    
    Fairy_Ezha "No I had no idea! I didn't even know there WAS a baby!"
    
    show ezha stunned2
    show lehina unsure2
    
    Fairy_Lehina "Oh no! Do you think you have enough time to get a gift together?"

    show lehina unsure3
    show ezha thinking

    $ renpy.pause( 1.0 )

    show ezha nervous
    Fairy_Ezha "Yeah, I think so. I should get going home though if I want to be prepared!"

    show lehina smile
    show ezha nervous2
    
    Fairy_Lehina "Okay! I'll let you get going then. I will see you there!"

    show lehina smile2
    show ezha proud
    
    Fairy_Ezha "Yeah!"
    
    hide ezha with dissolve
    hide lehina with dissolve
    show bg black with dissolve

    Fairy_Ezha "Wow! What amazing news!"
    Fairy_Ezha "I gotta get home and get something prepared!!"

    $ lehina += 1
    
    jump sb_ch1_home
    

########################################################################
#                                                              LIBRARY #
########################################################################
label sb_ch1_library:
    $ oyimahina_seeAtLibrary = True
    $ oyimahina += 1    

    show bg black with dissolve
    
    play sound "assets/sounds/392233__gropkcor__church-door-interior-throwleigh-sel.ogg"
    
    $ renpy.pause( 1.0 )
    
    play music "assets/music/Perspectives.mp3"
    show bg village_library with dissolve
    show ezha neutral with dissolve
    
    show ezha nervous
    Fairy_Ezha "Ack, that door is heavy."
    
    show ezha charmed
    Fairy_Ezha "Oh but I love the smell of old books here."
    
    show ezha thinking2
    Fairy_Ezha "Let's see, I'd like to pick up a book on ancient fairy languages, and on patchwork potions..."
    
    hide ezha with dissolve
    
    $ renpy.pause( 1.0 )
    
    show ezha neutral with dissolve

    show ezha smile
    Fairy_Ezha "Will I read these or not? Who knows! We will see!"

    Fairy_Ezha "Time to check out!"
    
    show ezha neutral

    show oyimahina neutral at Position(xpos=-1.0)

    $ renpy.pause( 0.2 )
    
    show ezha neutral
    show oyimahina sideways at left
    with move

    Fairy_Ezha "Oh, hey!"
    
    show ezha wave
    Fairy_Ezha "Good morning, [Oyimahina_Nickname]. How are you doing today?"

    show oyimahina sideways2
    show ezha neutral
    
    show ezha smile
    Fairy_Ezha "What's that book? \"Ban bini\"?" 

    show ezha charmed    
    Fairy_Ezha "Are you making a trait potion for a newborn?"

    show ezha charmed2
    show oyimahina talking

    Fairy_Oyimahina "Hello, Ezha. Yes, I need to put the finishing touches on a gift for a newborn."

    show oyimahina neutral
    show ezha charmed3 at center with move
    
    Fairy_Ezha "What an honor for the family!"
    Fairy_Ezha "Who are the parents?"

    show oyimahina talking

    Fairy_Oyimahina "The parents are King [King_Name] and Queen [Queen_Name]."

    show oyimahina neutral
    show ezha stunned with hpunch

    Fairy_Ezha "Wait, what? They've had a child?"

    show ezha stunned2
    show oyimahina talking

    Fairy_Oyimahina "Yes, the queen recently gave birth to a baby and there is a banquet tonight."
    Fairy_Oyimahina "The other fairies and I will be there tonight to bequeath our respective gifts."

    show oyimahina neutral
    show ezha crying
    
    Fairy_Ezha "I never got an invite! I haven't had any time to prepare a gift!"
    
    show ezha crying2
    show oyimahina sidelook

    Fairy_Oyimahina "That is understandable. I assume that the King and Queen did not want you to bestow what you consider a gift to their child."

    show oyimahina sideways2
    show ezha annoyed2
    
    Fairy_Ezha "That's nonsense; all kingdom fairies are given the chance to give gifts!"

    show ezha mad
    show oyimahina sideways3

    Fairy_Oyimahina "That may be so, but you might also consider whether your presence is wanted, and to consider respecting their wishes."
    
    show ezha bewildered
    
    Fairy_Ezha "Uhh..."

    Fairy_Oyimahina "I am leaving first.{p}Goodbye."
    Fairy_Ezha "Oh, okay."

    hide oyimahina sideways with moveoutleft
    show ezha at left with move

    show ezha thinking
    
    Fairy_Ezha "Hmm."
    
    hide ezha with dissolve
    hide oyimahina with dissolve
    show bg black with fade

    Fairy_Ezha "Well, this is some pretty big news!"
    Fairy_Ezha "I'm not sure if I explicitly wasn't invited, or if my invite got lost in the mail..."
    Fairy_Ezha "...But I'm a fairy, dangit, and it's my right to bequeath a gift to the royal offspring!"
    Fairy_Ezha "Plus I'm excited to see the new baby and give it a gift!!"

    $ oyimahina += 1
    
    jump sb_ch1_home

########################################################################
#                                                                 HOME #
########################################################################
label sb_ch1_home:

    #play music "assets/music/Somewhere Sunny.mp3"
    #show bg ezha_house_inner with dissolve
    
    #"Once I get home I know exactly what kind of gifts to make for a young princess."
    
    #"I mix a bit of this and that, paying attention to the sun's position in the sky, excitedly anticipating the banquet."
    
    #"Gifts all finished, I change into my fancy gown and leave for the castle."
    
    #show bg black with fade
    
    jump sb_ch1_banquet
