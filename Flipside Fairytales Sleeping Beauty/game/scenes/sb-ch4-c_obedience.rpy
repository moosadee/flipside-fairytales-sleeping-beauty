########################################################################
#                                                    TRIAL - OBEDIENCE #
########################################################################
label sb_ch3_trial2:
    scene bg black with fade
#   * Obedience     -   Has to learn to say "no" to peoples' demands of her...
#                       Some monster is sad that 'princesses never give him a chance',
#                       tries to guilt you into going with him.

    # The princess wanders through a deep, dark forest.
    Princess "I have no idea where I have to go...{w} Why did I have to break my Bravery gift first?"
    Princess "Choosing to be brave is so much harder."

    # Twig snap
    Princess "Eek!"

    # There's a shadow in the distance behind her
    Princess "Who are you?"

    # The monster appears, a face-changing morphing monster that shows
    # different faces to different people. This monster tries to isolate you from your support group,
    # guilts you into doing what he wants.

    # His first face: A friend. But slowly gets more menacing looking.
    Monster1 "Hello, Princess."
    Princess "Uh, hi."
    Monster1 "I see you are on a quest. May I accompany you?"
    Princess "...{w}I don't know you."
    Monster1 "That's alright. I just want to make sure you get through this forest safely."
    Princess "..."
    Monster1 "Come with me, I'll show you the way."

    # The princess follows the monster through the forest. Fade out/in.
    Monster1 "You know...{w} That fairy...{w} You shouldn't trust her."
    Princess "Duthaha?{p}I trust her. She's from my fiancé's kingdom."
    Monster1 "She doesn't have your best interests at heart.{p}You shouldn't speak to her."
    Princess "But--"
    Monster1 "I'll protect you, though."

    # Fade out/in. You're finally through the forest.
    Princess "Oh, the edge of the forest!"
    Princess "I should be fine from here, sir."
    Monster1 "..."
    Monster1 "I will keep travelling with you."

    menu:
        "But you said you'd only show me through the forest.":
            Princess "...{w}But you said you'd only show me through the forest."
            Monster1 "I think we've grown close.{w} I don't want to just abandon you."

        "I don't want you to come with me.":
            Princess "...{w}I don't want you to come with me."
            Monster1 "I think it's for the best, Princess. I'm coming with you."

        "You can't come with me.":
            Princess "...{w}You can--{w}"
            Monster1 "I am coming with you."
            Princess "..."


    # They reach a town and walk through it.
    Princess "I'm not sure what I should be looking for here."
    Monster1 "Just beware of these people, Princess."
    Princess "What? Why?"
    Monster1 "I don't trust them. They will be out to hurt you."
    Princess "Surely there are some good-"
    Monster1 "{b}Do not talk to anyone.{/b}"
    Princess "..."

    # Fade out/in.
    Princess "I'm so tired.{w} And it's getting late."

    menu:
        "I think it's time we part ways.":
            Princess "I think it's time we part ways."

        "I'm going back to the town.":
            Princess "I'm going back to the town."

        "You need to leave.":
            Princess "You need to leave."

    Princess "Thank you for your help, but I really need to do this on my own."

    # Monster continues changing
    Monster1 "Nonsense, Princess. I'll be here for you.{p}I think we've grown quite close."
    Princess "I want to travel alone..."
    Monster1 "But you just said you were tired."
    Monster1 "It's okay, Princess. I know where we can stay for the night.{w} Just you and me."
    Princess "Erk--"
    Monster1 "Please follow me."

    $ refusal_counter = 0
    $ dont_want = 0

label sb_ch3_trial2_refusal:
    menu:
            
        "I don't want to." if dont_want is 0:
            $ dont_want += 1
            Princess "I don't want to."
            Monster1 "Just give me a chance, Princess."

        "I don't want to give you a chance." if dont_want is 1:
            $ dont_want += 1
            Princess "I don't want to give you a chance."
            Monster1 "Princess, I'm a {i}nice guy.{/i}{p}Just spend some time with me, you'll see it."

        "I don't owe you anything." if dont_want is 2:
            $ dont_want += 1
            Princess "I don't owe you anything."
            Monster1 "Girls never give me a chance.{w} It is so unfair.{p}I'm really a nice guy."

        "No." if refusal_counter is 0 and dont_want is 2:
            $ refusal_counter += 1
            Princess "No. I won't come with you."
            Monster1 "But we're already close by!"
            Monster1 "At least stick with me until I get there."
            
        "I said NO." if refusal_counter is 1:
            $ refusal_counter += 1
            Princess "I said NO."
            Monster1 "So heartless, Princess. I'm just trying to be nice."
            Monster1 "Why do you want to attack me like this?"
            
        "I WILL NOT GO WITH YOU." if refusal_counter is 2:
            $ refusal_counter += 1
            Princess "I WILL NOT GO WITH YOU."
            # The monster continues morphing to be more scary
            Monster1 "You're such a BITCH."
            Monster1 "I don't know why I even TRY to be nice."
            Monster1 "NOBODY could LIKE a BITCH like YOU."

            Princess "I DON'T CARE.{w} LEAVE ME ALONE."
            Monster1 "AFTER ALL I'VE DONE FOR YOU.{p}YOU COULD AT LEAST BE KIND TO ME."
            Princess "I DON'T OWE YOU ANYTHING."
            Monster1 "I DESERVE YOUR ATTENTION."
            Princess "NOBODY.{w} OWES.{w} YOU.{w} ANYTHING."

            # The monster looks to be in pain and disappates.
            # Another gift appears and shatters.
            # The princess, alone, sits down and rests her head on her legs.

            Fairy_Duthaha "It's over now, Princess."
            Princess "Duthaha...{w} I feel...{w} disgusting."
            Princess "I felt trapped."
            Princess "I was so scared."
            Fairy_Duthaha "You don't have to go through that again."
            Princess "Why did you make me do that?"
            Fairy_Duthaha "Princess, I am not in control here.{w} This is a battle inside yourself."
            Fairy_Duthaha "I know that was difficult."
            # Hug
            Fairy_Duthaha "I'm here for you. Let's just take a break."
            jump sb_ch3_interlude
            
        "...Okay. I'll give you a chance." if dont_want > 0:
            Princess "...Okay. I'll give you a chance."
            Monster1 "Finally."

            # The monster "absorbs" the princess? Fade to black.
            Monster1 "Your body is mine, Princess."
            jump sb_ch3_trial2

    jump sb_ch3_trial2_refusal


label sb_ch3_interlude:
    # Back to Ezha and the fairies in the bedroom of the princess.
    # It is now sunset.
    Fairy_Ezha      "Are you okay, [Duthaha_Name]?"
    Fairy_Duthaha   "Yes, I'm just supporting the Princess with my energy."
    Fairy_Duthaha   "We're both quite tired."
    Fairy_Ezha      "Is there anything I can do?"
    Fairy_Duthaha   "Ezha, if you could, I need a meal to regain my strength.{p}I do not want to leave the Princess' side."
    Fairy_Ezha      "Got it! I'll go find you something."

    # Down in the dining room, the king and queen are eating dinner.
    # Whoever you're connected with most is out on the balcony.
    # You can have a little private time with them.

    # Return the food back to Duthaha and the dream continues.
