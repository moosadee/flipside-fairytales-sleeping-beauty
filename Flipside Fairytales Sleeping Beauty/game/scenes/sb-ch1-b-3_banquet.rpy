########################################################################
#                                                              BANQUET #
########################################################################
label sb_ch1_b3_banquet:

    hide ezhagown with moveoutleft
    hide guard with dissolve

    stop music
    show bg black with dissolve

    $ renpy.pause( 1.0 )

    play sound "assets/sounds/406682__tlcolbe__walking-in-heels-medium-step.ogg"

    $ renpy.pause( 1.0 )

    Fairy_Ezha      "Phew, stairs are NOT my friend."

    play music "assets/music/Ashton Manor.mp3"
    show bg black with dissolve


    show bg castle_banquet with dissolve

    show ezhagown neutral at center with dissolve

    Fairy_Ezha      "Ahh, I'm a little late. The feast has already started."

    show ezhagown nervous
    Fairy_Ezha      "Well, that's okay... I guess I'm just fashionably-late.{w} I mean... technically it's not {i}my fault{/i}."

    show ezhagown nervous2
    Fairy_Ezha      "But, I'll let this slide. This is King [King_Name] and Queen [Queen_Name]'s first kid after all."
    Fairy_Ezha      "Next time I hope they make sure all their invites are sent properly."

    hide ezhagown with dissolve

    show bg castle_banquet_tbl with dissolve

    show ezhagown neutral at right with moveinright

    show ezhagown proud
    Fairy_Ezha      "Hey there, ladies!"

    show shahinagown neutral at Position( xpos=0.0, xanchor='left' ) with dissolve

    Fairy_Ezha      "[Shahina_Name]..."

    show oyimahinagown neutral at Position( xpos=0.2, xanchor='left' ) with dissolve

    Fairy_Ezha      "[Oyimahina_Name]..."

    show lehinagown neutral at Position( xpos=0.4, xanchor='left' ) with dissolve

    Fairy_Ezha      "And [Lehina_Name]..."

    if ( shahina >= 1 ):
        show shahinagown annoyed2
        Fairy_Shahina   "H-- hi there..."

        show lehinagown smile2
        show shahinagown annoyed3
        Fairy_Lehina    "Hi, [Ezha_Name]!"

        show lehinagown neutral

    elif ( oyimahina >= 1 ):
        show oyimahinagown sidelook2
        Fairy_Oyimahina "Did I not tell you that you are not welcome here?"

        show lehinagown nervous3
        show oyimahinagown sidelook
        Fairy_Lehina    "Oyima!! You're so mean sometimes."

        show lehinagown nervous

    elif ( lehina >= 1 ):
        show lehinagown smile2
        show oyimahinagown sidelook
        Fairy_Lehina    "Welcome, [Ezha_Name]!"
        Fairy_Shahina   "..."
        Fairy_Oyimahina "..."

        show lehinagown neutral

    show ezhagown talking
    Fairy_Ezha      "Thanks for leaving space for me!"

    $ sitting = "none"

    menu:
        Fairy_Ezha  "Where do I want to put my butt?"

        "Between [Shahina_Name] {image=assets/ui/icon-shahina.png} and [Oyimahina_Name] {image=assets/ui/icon-oyimahina.png}":
            $ shahina += 1
            $ oyimahina += 1
            $ shahina_sitBy = True
            $ sitting = "SO"

            hide ezhagown
            hide lehinagown
            hide shahinagown
            hide oyimahinagown with dissolve

            show bg castle_table_close with dissolve

            hide lehina
            show oyimahinagown neutral at right
            show shahinagown neutral at left
            show ezhagown neutral at center with dissolve

        "Between [Oyimahina_Name] {image=assets/ui/icon-oyimahina.png} and [Lehina_Name] {image=assets/ui/icon-lehina.png}":
            $ oyimahina += 1
            $ lehina += 1
            $ lehina_sitby = True
            $ sitting = "OL"

            hide ezhagown
            hide lehinagown
            hide shahinagown
            hide oyimahinagown with dissolve

            show bg castle_table_close with dissolve

            hide shahinagown
            show lehinagown neutral at right
            show oyimahinagown neutral at left
            show ezhagown neutral at center with dissolve


    show ezhagown talking
    Fairy_Ezha      "Alright! Where's the waiter? I'm ready to dig into some good food!"
    show ezhagown proud
    Fairy_Ezha      "Banquets are the best! You don't get to eat like this every day!"
    show ezhagown neutral

    if ( sitting == "SO" ):
        show ezhagown talking
        Fairy_Ezha      "Soooo..."

        show ezhagown neutral
        show oyimahinagown sidelook2F
        Fairy_Oyimahina "I hope that you did not bring a gift for the infant."

        show ezhagown innocent
        show oyimahinagown sidelookF
        Fairy_Ezha      "What? Of course I did!"

        show ezhagown innocent2
        show oyimahinagown sidelook2F
        Fairy_Oyimahina "The king and queen specifically {i}did not{/i} invite you so that they would not have to deal with your {i}gifts{/i}."

        show ezhagown nervous
        show oyimahinagown sidelookF
        Fairy_Ezha      "Royalty is just like that, but it's a tradition. I MUST give my gifts."

        show ezhagown neutral
        show oyimahinagown sidelook2F
        Fairy_Oyimahina "There is no requirement that you {i}must--{/i}"

        show ezhagown mad
        show oyimahinagown sidelookF
        Fairy_Ezha      "Gifts are a MUST!!"

    elif ( sitting == "OL" ):
        show lehinagown talking
        Fairy_Lehina    "Here [Ezha_Name], I saved you a strawberry cupcake. The desserts have already been obliterated; you know how dwarves are."

        show lehinagown neutral
        show ezhagown talking
        Fairy_Ezha      "Oh, thank you, [Lehina_Name]! I love cupcakes!"

        show lehinagown talking
        show ezhagown neutral
        Fairy_Lehina    "So what are you giving the princess, [Ezha_Name]?"

        show lehinagown neutral
        show ezhagown innocent
        Fairy_Ezha      "Oh it's a secret, you'll have to wait and see!"

        show ezhagown innocent2
        show oyimahinagown sidelook2
        Fairy_Oyimahina "It is not wise to rush when making a gift for a newborn. You ought to really think through it."

        show oyimahinagown sidelook
        show ezhagown mad
        Fairy_Ezha      "Of course! I'm not some amateur fairy, after all."

        show lehinagown talking
        show ezhagown neutral
        Fairy_Lehina    "Well {i}I{i} can't wait to see."

    show ezhagown talking
    Fairy_Ezha      "OK, time to dig into--"

    show ezhagown surprised
    show semishade with dissolve
    play sound "assets/sounds/423455__ohforheavensake__trumpet-brass-fanfare.ogg"

    centered "The proclaimation horns begin playing to announce the royal family's arrival."
    hide semishade with dissolve

    # Horns
    play music "assets/music/Egmont Overture.mp3"

    show ezhagown nervous
    Fairy_Ezha          "Aw, dang!"

    show ezhagown nervous3
    Page                "His Royal Highness will now speak!"

    hide ezhagown
    hide lehinagown
    hide shahinagown
    hide oyimahinagown

    show bg castle_throne with dissolve

    show queen neutral at Position(xpos=0.4)
    show king neutral at Position(xpos=0.6) with dissolve

    show king talking
    King    "I thank you all for attending the banquet today."

    show king neutral
    show queen talking
    Queen   "We are here to celebrate the birth of the royal princess [Princess_Name]."

    show king talking
    show queen neutral
    King    "We thank you for your gifts to help bless our new baby girl."
    King    "We trust that you've put much thought into your gifts to make sure that they are... {w} {i}appropriate{/i}...{w} for our daughter."

    hide king
    hide queen
    show bg castle_bassinet with dissolve

    Page    "We will now permit one person at a time to view the child, in order to bestow their gifts."


    #"The humans, dwarves, and us fairies line up for a chance to see the new child."
    #"The humans bestow physical gifts, such as soft-cloth dolls and rattles. The dwarves present elaborate woven blankets and delicate little baby dresses."
    #"Then, it's our turn. I sit at the back of the line and the other fairies go first."

    # BRAVERY:      Lion plushie
    # KINDNESS:     Bluebell flower
    # OBEDIENCE:    Platycodon flower
    # BEAUTY:       Amaryllis flower

    show shahinagown spell at left with dissolve
    show shahinagown spell2
    Fairy_Shahina   "Sweet child, I bestow upon you the gift of beauty."
    #"Shahina sprinkles her rose-scented magic dust over the child."
    show shahinagown spell
    show bg castle_bassinet1 with dissolve
    hide shahinagown with dissolve

    show oyimahinagown spell at left with dissolve
    show oyimahinagown spell2
    Fairy_Oyimahina "Young one, I bestow upon you the gift of obedience."
    #"Oyimahina takes an oily potion that she concocted and places some drops on the baby's forehead."
    show oyimahinagown spell
    show bg castle_bassinet2 with dissolve
    hide oyimahinagown with dissolve

    show lehinagown spell at left with dissolve
    show lehinagown spell2
    Fairy_Lehina "Little girl, I bestow upon you the gift of kindness."
    #"Lehina simply lays a white crocus flower atop the baby's belly."
    show lehinagown spell
    show bg castle_bassinet3 with dissolve
    hide lehinagown with dissolve

    show ezhagown neutral at left with dissolve
    Fairy_Ezha  "{size=-5}(My turn!){/size}"
    show ezhagown nervous
    # Update king/queen graphics
    Fairy_Ezha  "{size=-5}(Woah, the king and queen {i}do not{/i} look happy.){/size}"

    if sitting == "SO":
        Fairy_Ezha "{size=-5}(Maybe [Oyimahina_Nickname] was right...){/size}"


    show ezhagown neutral
    Fairy_Ezha  "{size=-5}(OK, time to do it!){/size}"

    show ezhagown spell3
    Fairy_Ezha  "Tiny child who will grow to hold much power, I bestow upon you..."
    Fairy_Ezha  "{size=-5}(...Pause for effect...){/size}"

    show queen neutral at right with moveinright
    $ renpy.pause( 0.5 )

    show queen angry
    Queen       "*grumbles*"

    # "The page speaks in a low tone to me in warning."

    Fairy_Ezha  "...the gift of bravery!"
    show ezhagown spell4 with dissolve
    show bg castle_bassinet4 with dissolve

    $ renpy.pause( 0.5 )
    show queen yell with vpunch
    Queen       "{size=+5}[Ezha_Name]!!{/size}"
    show queen teethclench
    Queen       "{size=-5}That's not a very {i}lady-like{/i} gift to give a young girl...{/size}"

    show ezhagown proud
    Fairy_Ezha  "Even a little girl needs bravery, your highness."

    show ezhagown proud2
    show queen yell with vpunch
    Queen       "You UNDO that gift THIS INSTANT!"

    show queen teethclench
    show ezhagown nervous
    Fairy_Ezha  "Sorry, your majes--"

    show ezhagown nervous4
    show queen yell with vpunch
    Queen       "This is NOT the gift I want for MY little girl!"

    # King shows up
    show queen teethclench at Position(xpos=0.8) with move
    show king neutral at right with moveinright
    show king talking
    King        "[Ezha_Name], you {i}knew{/i} that we didn't want {b}you{/b} here.{p}We didn't want {b}your{/b} gift."

    show king neutral
    show ezhagown nervous
    Fairy_Ezha  "I thought that perhaps my invitation had been lost in the mail, sire."

    show king talking
    show ezhagown nervous4
    King        "You don't {i}honestly{/i} believe that, [Ezha_Name]. You decided to {b}knowingly{/b} go against our wishes."

    show king neutral
    show ezhagown nervous
    Fairy_Ezha  "Aww, come on, [King_Name]. I was there for {i}your{/i} gifting as a baby!"

    show king talking
    show ezhagown nervous4
    King        "It does not matter how long you've served our kingdom. You show surprisingly poor judgement and a complete lack of tact."

    King        "You're not welcome in Juvela any more."

    # Gasping sound
    # Show Ezha and the fairies together, hide king/queen

    show ezhagown upset
    Fairy_Ezha          "...King [King_Name]!"

    # Hide fairies, show king/queen

    show king closedeyes
    King                "Please leave."

    show king closedeyes2
    show ezhagown crying
    Fairy_Ezha          "..."

    Fairy_Ezha          "I have to leave? [Juvela_Kingdom]?"

    show king closedeyes
    King                "Yes. You are not welcome here anymore."
    show king closedeyes2

    hide ezhagown with moveoutleft

    show queen teethclench at center with move

    show queen yell with vpunch
    Queen               "FAIRIES! FIX THIS!"

    show queen teethclench at center with move


    show oyimahinagown neutral at left with moveinleft
    show oyimahinagown talking
    Fairy_Oyimahina     "Your highness, I apologize but it is not in our power to remove enchantments created by {i}other{/i} fairies."

    show oyimahinagown neutral
    Queen               "..."

    hide queen
    hide oyimahinagown
    hide king
    with dissolve

    show bg black with fade

    $ renpy.pause( 1.0 )

    if demo == True:
        jump end_of_demo

    jump sb_ch1_banishment



label end_of_demo:
    centered "That's it for the demo! This game is still a work in progress, but thank you for trying it out!"
    centered "Go to moosader.itch.io to watch the game development log and download other games! :)"

    return
