label sb_init:

    image semishade = "#0006"
    image bg chapter_frame      = "assets/background/chapter-frame.jpg"

    $ demo = True

########################################################################
#                                                    BACKGROUND IMAGES #
########################################################################

    image bg ezha_house_outer   = "assets/background/ezha-house-outer.jpg"
    image bg ezha_house_inner   = "assets/background/ezha-house-internal.jpg"
    image bg ezha_house_moving  = "assets/background/ezha-house-internal-moveout.jpg"
    image bg ezha_kitchen_moving  = "assets/background/ezha-house-kitchen-moveout.jpg"
    image bg village_library    = "assets/background/village-library.jpg"
    image bg village_market     = "assets/background/village-market.jpg"
    image bg village_park       = "assets/background/village-park.jpg"
    image bg castle_banquet     = "assets/background/castle-banquet.jpg"
    image bg castle_outer       = "assets/background/castle-outer.jpg"
    image bg castle_banquet_tbl = "assets/background/banquet-table.jpg"
    image bg castle_banquet_tblA = "assets/background/banquet-tableA.jpg"
    image bg castle_banquet_tblB = "assets/background/banquet-tableB.jpg"
    image bg castle_table_close = "assets/background/banquet-tableClose.jpg"
    image bg castle_throne      = "assets/background/castle-throne.jpg"
    image bg castle_bassinet    = "assets/background/castle-bassinet.jpg"
    image bg castle_bassinet1   = "assets/background/castle-bassinet1.jpg"
    image bg castle_bassinet2   = "assets/background/castle-bassinet2.jpg"
    image bg castle_bassinet3   = "assets/background/castle-bassinet3.jpg"
    image bg castle_bassinet4   = "assets/background/castle-bassinet4.jpg"
    image bg castle_bedroom     = "assets/background/castle-bedroom.jpg"
    image bg castle_kitchen     = "assets/background/castle-kitchen.jpg"
    image bg castle_tearoom     = "assets/background/castle-tearoom.jpg"

    image bg dream_path         = "assets/background/dream-path.jpg"

########################################################################
#                                                     CHARACTER IMAGES #
########################################################################
    #                                                             Ezha #
    ####################################################################
    image ezha pajamas          = "assets/characters/ezha-pajamas.png"
    image ezha pjyawn           = "assets/characters/ezha-pj-yawn.png"
    image ezha neutral          = "assets/characters/ezha-neutral.png"
    image ezha thinking         = "assets/characters/ezha-thinking.png"
    image ezha thinking2        = "assets/characters/ezha-thinking2.png"
    image ezha smile            = "assets/characters/ezha-smile.png"
    image ezha crying           = "assets/characters/ezha-crying.png"
    image ezha crying2          = "assets/characters/ezha-crying2.png"
    image ezha bewildered       = "assets/characters/ezha-bewildered.png"
    image ezha bewildered2      = "assets/characters/ezha-bewildered2.png"
    image ezha nervous          = "assets/characters/ezha-nervous.png"
    image ezha nervous2         = "assets/characters/ezha-nervous2.png"
    image ezha wave             = "assets/characters/ezha-wave.png"
    image ezha spiders          = "assets/characters/ezha-spiders.png"
    image ezha magic            = "assets/characters/ezha-magic.png"
    image ezha proud            = "assets/characters/ezha-proud.png"
    image ezha skeptical        = "assets/characters/ezha-skeptical.png"
    image ezha mad              = "assets/characters/ezha-mad.png"
    image ezha yell             = "assets/characters/ezha-yell.png"
    image ezha yell2            = "assets/characters/ezha-yell2.png"
    image ezha yell3            = "assets/characters/ezha-yell3.png"
    image ezha annoyed          = "assets/characters/ezha-annoyed.png"
    image ezha annoyed2         = "assets/characters/ezha-annoyed2.png"
    image ezha charmed          = "assets/characters/ezha-charmed.png"
    image ezha charmed2         = "assets/characters/ezha-charmed2.png"
    image ezha charmed3         = "assets/characters/ezha-charmed3.png"
    image ezha stunned          = "assets/characters/ezha-stunned.png"
    image ezha stunned2         = "assets/characters/ezha-stunned2.png"
    image ezha depressed        = "assets/characters/ezha-depressed.png"
    image ezha depressed2       = "assets/characters/ezha-depressed2.png"
    image ezha depressed3       = "assets/characters/ezha-depressed3.png"
    image ezha solemn           = "assets/characters/ezha-solemn.png"
    image ezha solemn2          = "assets/characters/ezha-solemn2.png"

    image ezhagown neutral      = "assets/characters/ezha-gown-neutral.png"
    image ezhagown talking      = "assets/characters/ezha-gown-talking.png"
    image ezhagown proud        = "assets/characters/ezha-gown-proud.png"
    image ezhagown proud2       = "assets/characters/ezha-gown-proud2.png"
    image ezhagown thinking     = "assets/characters/ezha-gown-thinking.png"
    image ezhagown mad          = "assets/characters/ezha-gown-mad.png"
    image ezhagown yell         = "assets/characters/ezha-gown-yell.png"
    image ezhagown yell2        = "assets/characters/ezha-gown-yell2.png"
    image ezhagown yell3        = "assets/characters/ezha-gown-yell3.png"
    image ezhagown annoyed      = "assets/characters/ezha-gown-annoyed.png"
    image ezhagown spiders      = "assets/characters/ezha-gown-spiders.png"
    image ezhagown skeptical    = "assets/characters/ezha-gown-skeptical.png"
    image ezhagown magic        = "assets/characters/ezha-gown-magic.png"
    image ezhagown spell        = "assets/characters/ezha-gown-spell.png"
    image ezhagown spell2       = "assets/characters/ezha-gown-spell2.png"
    image ezhagown spell3       = "assets/characters/ezha-gown-spell3.png"
    image ezhagown spell4       = "assets/characters/ezha-gown-spell4.png"
    image ezhagown innocent     = "assets/characters/ezha-gown-innocent.png"
    image ezhagown innocent2    = "assets/characters/ezha-gown-innocent2.png"
    image ezhagown hero         = "assets/characters/ezha-gown-savedtheday.png"
    image ezhagown nervous      = "assets/characters/ezha-gown-nervous.png"
    image ezhagown nervous2     = "assets/characters/ezha-gown-nervous2.png"
    image ezhagown nervous3     = "assets/characters/ezha-gown-nervous3.png"
    image ezhagown nervous4     = "assets/characters/ezha-gown-nervous4.png"
    image ezhagown surprised    = "assets/characters/ezha-gown-surprised.png"
    image ezhagown upset        = "assets/characters/ezha-gown-upset.png"
    image ezhagown crying       = "assets/characters/ezha-gown-crying.png"

    ####################################################################
    #                                                          Shahina #
    ####################################################################
    image shahina neutral       = "assets/characters/shahina-neutral.png"
    image shahina side          = "assets/characters/shahina-side.png"
    image shahina uncomfy       = "assets/characters/shahina-uncomfortable.png"
    image shahina nervous       = "assets/characters/shahina-nervous.png"
    image shahina nervous2      = "assets/characters/shahina-nervous2.png"
    image shahina nervous3      = "assets/characters/shahina-nervous3.png"
    image shahina nervous4      = "assets/characters/shahina-nervous4.png"
    image shahina annoyed       = "assets/characters/shahina-annoyed.png"
    image shahina annoyed2      = "assets/characters/shahina-annoyed2.png"

    image shahinagown neutral   = "assets/characters/shahina-gown-neutral.png"
    image shahinagown joy       = "assets/characters/shahina-gown-joy.png"
    image shahinagown joy2      = "assets/characters/shahina-gown-joy2.png"
    image shahinagown spell     = "assets/characters/shahina-gown-spell.png"
    image shahinagown spell2    = "assets/characters/shahina-gown-spell2.png"
    image shahinagown annoyed   = "assets/characters/shahina-gown-annoyed.png"
    image shahinagown annoyed2  = "assets/characters/shahina-gown-annoyed2.png"
    image shahinagown annoyed3  = "assets/characters/shahina-gown-annoyed3.png"
    image shahinagown nervous   = "assets/characters/shahina-gown-nervous.png"
    image shahinagown nervous2  = "assets/characters/shahina-gown-nervous2.png"
    image shahinagown nervous3  = "assets/characters/shahina-gown-nervous3.png"
    image shahinagown annoyedF   = im.Flip( "assets/characters/shahina-gown-annoyed.png", horizontal=True )
    image shahinagown annoyedF2  = im.Flip( "assets/characters/shahina-gown-annoyed2.png", horizontal=True )

    ####################################################################
    #                                                        Oyimahina #
    ####################################################################
    image oyimahina neutral     = "assets/characters/oyimahina-neutral.png"
    image oyimahina sidelook    = "assets/characters/oyimahina-sidelook.png"
    image oyimahina sidelook2   = "assets/characters/oyimahina-sidelook2.png"
    image oyimahina sidelook3   = "assets/characters/oyimahina-sidelook3.png"
    image oyimahina talking     = "assets/characters/oyimahina-talking.png"
    image oyimahina sideways    = "assets/characters/oyimahina-sideways.png"
    image oyimahina sideways2   = "assets/characters/oyimahina-sideways2.png"
    image oyimahina sideways3   = "assets/characters/oyimahina-sideways3.png"

    image oyimahinagown neutral   = "assets/characters/oyimahina-gown-neutral.png"
    image oyimahinagown spell     = "assets/characters/oyimahina-gown-spell.png"
    image oyimahinagown spell2    = "assets/characters/oyimahina-gown-spell2.png"
    image oyimahinagown talking   = "assets/characters/oyimahina-gown-talking.png"
    image oyimahinagown sidelook  = "assets/characters/oyimahina-gown-sidelook.png"
    image oyimahinagown sidelook2 = "assets/characters/oyimahina-gown-sidelook2.png"
    image oyimahinagown sidelookF = im.Flip( "assets/characters/oyimahina-gown-sidelook.png", horizontal=True )
    image oyimahinagown sidelook2F = im.Flip( "assets/characters/oyimahina-gown-sidelook2.png", horizontal=True )

    ####################################################################
    #                                                           Lehina #
    ####################################################################
    image lehina neutral        = "assets/characters/lehina-neutral.png"
    image lehina smile          = "assets/characters/lehina-smile.png"
    image lehina smile2         = "assets/characters/lehina-smile2.png"
    image lehina talking        = "assets/characters/lehina-talking.png"
    image lehina nervous        = "assets/characters/lehina-nervous.png"
    image lehina nervous2       = "assets/characters/lehina-nervous2.png"
    image lehina unsure         = "assets/characters/lehina-unsure.png"
    image lehina unsure2        = "assets/characters/lehina-unsure2.png"
    image lehina unsure3        = "assets/characters/lehina-unsure3.png"
    image lehina sad            = "assets/characters/lehina-sad.png"
    image lehina sad2           = "assets/characters/lehina-sad2.png"

    image lehinagown neutral    = "assets/characters/lehina-gown-neutral.png"
    image lehinagown spell      = "assets/characters/lehina-gown-spell.png"
    image lehinagown spell2     = "assets/characters/lehina-gown-spell2.png"
    image lehinagown nervous    = "assets/characters/lehina-gown-nervous.png"
    image lehinagown nervous2   = "assets/characters/lehina-gown-nervous2.png"
    image lehinagown nervous3   = "assets/characters/lehina-gown-nervous3.png"
    image lehinagown sidelook   = "assets/characters/lehina-gown-sidelook.png"
    image lehinagown smile      = "assets/characters/lehina-gown-smile.png"
    image lehinagown smile2     = "assets/characters/lehina-gown-smile2.png"
    image lehinagown talking    = "assets/characters/lehina-gown-talking.png"
    image lehinagown talking2   = "assets/characters/lehina-gown-talking2.png"

    ####################################################################
    #                                                          Duthaha #
    ####################################################################
    image duthaha neutral       = "assets/characters/duthaha-neutral.png"

    ####################################################################
    #                                                            Queen  #
    ####################################################################
    image queen neutral         = "assets/characters/queen-neutral.png"
    image queen talking         = "assets/characters/queen-talking.png"
    image queen angry           = "assets/characters/queen-angry.png"
    image queen yell            = "assets/characters/queen-yell.png"
    image queen teethclench     = "assets/characters/queen-teethclench.png"

    ####################################################################
    #                                                             King #
    ####################################################################
    image king neutral          = "assets/characters/king-neutral.png"
    image king talking          = "assets/characters/king-talking.png"
    image king angry            = "assets/characters/king-angry.png"
    image king closedeyes       = "assets/characters/king-closedeyes.png"
    image king closedeyes2      = "assets/characters/king-closedeyes2.png"

    ####################################################################
    #                                                         Princess #
    ####################################################################
    image dreamprincess worried = "assets/characters/dream-princess-worried.png"
    image dreamprincess away = "assets/characters/dream-princess-lookaway.png"

    ####################################################################
    #                                                            Guard #
    ####################################################################
    image guard neutral         = "assets/characters/guard-neutral.png"
    image guard neutral2        = "assets/characters/guard-neutralnotalk.png"
    image guard read            = "assets/characters/guard-read.png"
    image guard read2           = "assets/characters/guard-read2.png"
    image guard worry           = "assets/characters/guard-worry.png"
    image guard worry2          = "assets/characters/guard-worry2.png"
    image guard worry2b         = "assets/characters/guard-worry2b.png"
    image guard worry3          = "assets/characters/guard-worry3.png"
    image guard worry3b         = "assets/characters/guard-worry3b.png"
    image guard worry4          = "assets/characters/guard-worry4.png"
    image guard worry4b         = "assets/characters/guard-worry4b.png"
    image guard spiders         = "assets/characters/guard-spiders.png"
    image guard petrified       = "assets/characters/guard-petrified.png"

    ####################################################################
    #                                                           Prince #
    ####################################################################

    image prince neutral        = "assets/characters/brandon-neutral.png"

    ####################################################################
    #                                                 Dream characters #
    ####################################################################



    ####################################################################
    #                                                         One-offs #
    ####################################################################
    image shopkeeper neutral    = "assets/characters/shopkeeper.png"
    image shopkeeper talking    = "assets/characters/shopkeeper-talking.png"

    image suwi neutral          = "assets/characters/suwi-neutral.png"

    image doctor neutral        = "assets/characters/doctor-neutral.png"

########################################################################
#                                                       NAME VARIABLES #
########################################################################

    $ Ezha_FullName = "Hodo Ozhá"
    $ Ezha_Name = "Ozhá"
    $ Ezha_Nickname = "Ozh-hee"

    $ Lehina_FullName = "Lehina Lalomá"
    $ Lehina_Name = "Lehina"
    $ Lehina_Nickname = "Leh-hee"

    $ Oyimahina_FullName = "Oyimahina Ulaniná"
    $ Oyimahina_Name = "Oyimahina"
    $ Oyimahina_Nickname = "Oyee-hee"

    $ Shahina_FullName = "Shahina Balishá"
    $ Shahina_Name = "Shahina"
    $ Shahina_Nickname = "Sha-hee"

    $ Duthaha_FullName = "Zhenedahina Duthahá"
    $ Duthaha_Name = "???"
    #$ Duthaha_Name = "Duthahá"

    $ King_FullName = "Altega Forteco de Juvela"
    $ King_Name = "Altega"

    $ Queen_FullName = "Saĝeca Jonkvilo de Juvela"
    $ Queen_Name = "Saĝeca"

    $ Princess_FullName = "Aminda Ĉarmulin de Juvela"
    $ Princess_Name = "Aminda"
    $ Princess_Name_Original = "Aminda"

    $ Prince_FullName = "Brando Robinio di Arjento"
    $ Prince_Name = "Brando"

    $ Juvela_Kingdom        = "Juvela Kingdom"
    $ Arjento_Kingdom       = "Arjento Kingdom"
    $ Fairy_Kingdom         = "Lolehoth"
    $ Fairy_Kingdom_eo      = "Feolando"
    $ Dream_Kingdom         = "aaa"

    $ Dwarf_Doctor          = "Suwi"
    $ The_Cook              = "Kukumo the Cook"
    $ The_Handmaid          = "Feliĉe the Handmaid"

########################################################################
#                                                CHARACTER DEFINITIONS #
########################################################################

    define Fairy_Ezha       = Character( "[Ezha_Name]",                 color="#b889f1" )
    define Fairy_Shahina    = Character( "[Shahina_Name]",              color="#ed8a8a" )
    define Fairy_Lehina     = Character( "[Lehina_Name]",               color="#72a5e4" )
    define Fairy_Oyimahina  = Character( "[Oyimahina_Name]",            color="#d1b03d" )
    define Fairy_Duthaha    = Character( "[Duthaha_Name]",              color="#ffe400")
    define Princess         = Character( "Princess [Princess_Name]",    color="#f9af6c" )
    define Queen            = Character( "Queen [Queen_Name]",          color="#d86666" )
    define King             = Character( "King [King_Name]",            color="#e0aaaa" )
    define Prince           = Character( "Prince [Prince_Name]",        color="#8a9dff" )
    define Guard            = Character( "Guard",                       color="#c6c6c6" )
    define Page             = Character( "Page" )
    define LIW              = Character( "Lady in Waiting" )
    define Friend           = Character( "Rozamik",                     color="#f4ce7d" )
    define Doctor           = Character( "Doctor",                      color="#ffd3d3" )
    define Suwi             = Character( "Suwi",                        color="#ff87ef" )
    define Dragon           = Character( "Dragon",                      color="#9ddf88" )
    define DwarfGirl        = Character( "Dwarven woman",               color="#000000" )
    define DwarfBoy         = Character( "Dwarven man",                 color="#000000" )
    define SoapboxGuy       = Character( "Soapbox guy" )
    define AngryGuy         = Character( "Angry guy" )
    define Shopkeeper       = Character( "Shopkeeper",                  color="#fffe8f" )
    define Cook             = Character( "[The_Cook]" )
    define Handmaid         = Character( "[The_Handmaid]" )
    define DreamPrince      = Character( "Dream Prince" )
    define DreamPrincess    = Character( "Dream Princess" )

    # Maybe temporary names
    define Monster1         = Character( "Sollarere" )                  # Obedience, Sollarere (solresol) [Oppress, stifle, oppression, suffocation, choking, stifling]
    define Monster2         = Character( "Fasolrefa" )                  # Kindness, Fasolrefa (solresol) [Attack, assail, offensive, assault, aggression, attacker, assailant, aggressor, aggressive]
    define Monster3         = Character( "" )                           # Beauty

########################################################################
#                                                       SPECIAL IMAGES #
########################################################################

    image spiders               = "assets/characters/spiders.png"
    image magic                 = "assets/characters/magic.png"
    image magic2                = "assets/characters/magic2.png"


    # gifts... regalness, loyalty, obedience, beauty, manners, kindness
    # gifts... intelligence, curiosity





########################################################################
#                                                  RELATIONSHIP EVENTS #
########################################################################

########################################################################
#                                                      STORY VARIABLES #
########################################################################

    #################
    # RELATIONSHIPS #
    #################

    $ shahina = 0       # Anxious
    $ lehina = 0        # Innocent, sweet
    $ oyimahina = 0     # Serious, aloof

    # Chapter 1
    $ lehina_seeAtLake = False                  # +1
    $ shahina_seeAtMarket = False               # +1
    $ oyimahina_seeAtLibrary = False            # +1

    $ shahina_sitBy = False                     # +1
    $ lehina_sitby = False                      # +1

    $ lehina_helpMove = False                   # +1
    $ shahina_helpMove = False                  # +1
    $ oyimahina_helpMove = False                # +1

    $ oyimahina_calledImpressive = False        # +1
    $ oyimahina_quietWhenCasting = False        # +1
    $ oyimahina_yodelingWhenCasting = False     # -1
    $ oyimahina_wantToTalk = False              # +2

    $ shahina_spritesLunch = False              # +1
    $ shahina_spritesClothes = False            # +1
    $ shahina_spritesNothing = False            # -1
    $ shahina_spritesClothesSecret = False      # +1
    $ shahina_spritesClothesShow = False        # +2

    $ lehina_appreciateLehina = False           # +1
    $ lehina_appreciateHouse = False            # +1
    $ lehina_appreciateNothing = False
    $ lehina_kiss = False                       # +4
    $ lehina_hug = False                        # +2
    $ lehina_alone = False

    $ lehina_lives_with_ezha = False

    # Chapter 2
    $ lehina_gotoCastle = False                 # +1
    $ shahina_gotoCastle = False                # +1
    $ oyimahina_gotoCastle = False              # +1

    # Chapter 4
    $ lehina_waiting_talk = False               # +1
    $ shahina_waiting_talk = False              # +1
    $ oyimahina_waiting_talk = False            # +1


    ############
    # GAMEPLAY #
    ############

    $ ch2_gowith = ""
    $ ch2_talkToPrince = False
    $ ch2_talkToHandmaid = False
    $ ch2_foundPotion = False
    $ ch2_talkedToDuthaha = False

    $ talkedToDoctor = False
    $ talkedToShahina = False
    $ talkedToDuthaha = False
    $ searchedBedroom = False

    $ talkedToKingQueen = False
    $ talkedToPrince = False
    $ talkedToLehina = False
    $ talkedToSprite = False

    $ talkedToCook = False
    $ talkedToHandmaid = False
    $ talkedToOyimahina = False
    $ searchedStaffroom = False

    $ impressPrince = False
    $ impressPrincess = False
    $ impressBoth = False
    $ impressNone = False

    $ pronounSet = "She" # He, They
    $ childTitle = "daughter"


    $ title = "Princess"

########################################################################
#                                                                POEMS #
########################################################################

    $ poem_dontwannawork = "Báa ham le nuha bebáawáan? / Bíi néde ra bodibod le wa. / Thi le mahela. / Néde dóhin le."

########################################################################
#                                                               RETURN #
########################################################################

    return
